import Image from 'next/image'
import '../app/globals.css'

export const metadata = {
  title: '404 Not Found'
}

export default function Error404() {
  return (
    <main className="bg-scheme-1 bg-none text-onscheme-1">
      <h1 className="m-auto mt-24 text-8xl text-center">404</h1>
      <p className="m-auto mt-2 text-2xl text-center">Not Found</p>
      <p className="m-auto mt-2 text-md text-center">The thing you sought could not be brought.</p>
      <p className="m-auto mt-2 text-md text-center">Maybe try <a className="text-primary-3 hover:underline" href="/">this instead</a>.</p>
    </main>
  )
}