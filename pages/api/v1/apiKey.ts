import ApiKey from "@/src/models/api/apikey";
import AuthPreflight from "@/src/preflight/auth";
import { customAlphabet, urlAlphabet } from "nanoid";
import { NextApiRequest, NextApiResponse } from "next";

const nanoidToken = customAlphabet('fed1234786905bac-', 64);
const nanoidClientId = customAlphabet(urlAlphabet.replace(/[-_A-Z]/,''), 6);

export default async function handler(req: NextApiRequest, res: NextApiResponse) {
  if (req.method != 'POST' && req.method != 'GET') {
    res.status(405).json({
      "error": "invalid",
      "message": "This endpoint only supports POST requests made with a cookie, or GET requests made with an API key."
    });
    return;
  }
  {const preflight = await AuthPreflight(req, res); if (preflight == false) return;}
  if (req.method == 'POST') {
    // API keys aren't allowed to create other API keys.
    if (req.headers.authorization) {
      res.status(403).json({
        "error": "forbidden",
        "message": "You are not allowed to create API keys with an API key."
      });
      return;
    }
    if (!req.body.name) {
      res.status(400).json({
        "error": "invalid",
        "message": "You need to specify a name for the API key, so you can remember what it belongs to."
      });
      return;
    }

    var apiKey = `warp-${nanoidClientId(5)}-${nanoidClientId(7)}-${nanoidClientId(5)}`;
    var apiToken = nanoidToken();

    const regenerateCodes = () => {
      apiKey = `warp-${nanoidClientId(5)}-${nanoidClientId(7)}-${nanoidClientId(5)}`;
      apiToken = nanoidToken();
    }

    var count = 0;
    while (await new ApiKey().findOne({client_id: apiKey, token: apiToken})) {
      if (count > 10) {
        res.status(500).json({
          "error": "internal-error",
          "message": "Could not generate an API key. Please try again later.",
          "technical": "Took more than 10 tries to find an unused code. It might help to increase the default length."
        });
        return;
      }
      regenerateCodes();
      count++;
    }

    const newData = {
      name: req.body.name,
      owner: req.headers['x-warp-user'] as string,
      client_id: apiKey,
      token: apiToken,
      created_at: new Date(Date.now()),
    };
    const key = await new ApiKey().insertOne(newData);
    if (!key) {
      res.status(500).json({
        "error": "internal-error",
        "message": "Could not create the API key. Please try again later."
      });
      return;
    }
    res.status(200).json({
      name: newData.name,
      client_id: newData.client_id,
      token: newData.token, // This will only be shown once, in this response.
      created_at: newData.created_at
    });
    return;
  } else {
    if (!req.headers.authorization) {
      // You can't check your own API key if you don't have one.
      res.status(405).json({
        "error": "invalid",
        "message": "You need an API key to perform a self-check. If you're trying to create an API key, send a POST instead."
      });
      return;
    }
    const key = await new ApiKey().findOne({token: req.headers.authorization.replace(/^Bearer /, '')});
    // if it's listed (and not expired), it's valid.
    // TODO: check if it's expired.
    if (!key) {
      res.status(404).json({
        "error": "not-found",
        "message": "Not implemented."
      });
      return;
    }
    res.status(200).json({
      name: key.name, // TODO: return "API Key" if the owner opts out of name sharing.
      client_id: key.client_id,
      created_at: key.created_at
    });
    return;
  }
}

export interface ApiKeyResponse {
  /** The owner-given name for the key. */
  name: string;
  /** The generated client ID for the key. */
  client_id: string;
  /** The generated token for the key. */
  token?: string;
  /** The date the key was created. */
  created_at: Date;
}