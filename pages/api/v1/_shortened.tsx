import { ShortenResponse } from "./shorten";

export default function ShortenedPage(data: ShortenResponse) {
  return <div className="flex flex-col gap-4">
    <h1 className="text-3xl">Link? Shortened.</h1>

    <label className="text-sm">Here&apos;s it short:</label>
    <input type="url" id="shortenedUrl" className="p-2 rounded-md border-2 border-scheme-2 focus:outline-none focus:border-primary-3 bg-scheme-2" value={data.link} readOnly />
  </div>;
}