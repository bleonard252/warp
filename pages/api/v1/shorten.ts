import AuthPreflight from '@/src/preflight/auth'
import { customAlphabet } from 'nanoid';
import type { NextApiRequest, NextApiResponse } from 'next';
import { validateIri, IriValidationStrategy } from 'validate-iri';
import { getDomainConfig } from '@/src/kdl';
import Shortlinked from '@/src/models/api/shortlinked';
import { ShortenedLinkType } from '@/src/models/shortlinked';

const randomShortener = customAlphabet('abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789', 6);

export default async function handler(req: NextApiRequest, res: NextApiResponse) {
  if (req.method != 'POST') {
    res.status(405).json({
      "error": "invalid",
      "message": "This endpoint only supports POST requests."
    });
    return;
  }
  {const preflight = await AuthPreflight(req, res); if (preflight == false) return;}
  if (!process.env.MONGODB_URI) {
    res.status(500).json({
      "error": "internal-error",
      "message": "The server is not configured correctly. Please contact the administrator.",
      "technical": "MONGODB_URI is not set."
    });
    return;
  }
  if (!req.body.long_url) {
    res.status(400).json({
      "error": "missing-required",
      "message": "The long_url parameter is required to be set to a valid URI."
    });
    return;
  }
  try {
    const error = validateIri(req.body.long_url, IriValidationStrategy.Pragmatic);
    if (error) throw error;
  } catch (e) {
    res.status(400).json({
      "error": "invalid",
      "message": "The long_url parameter is required to be set to a valid URI.",
      "technical": e?.message || e.toString()
    });
    return;
  }

  const domain = req.body.domain || process.env.PRIMARY_DOMAIN || 'unconfigured.example.com';
  const domainConfig = await getDomainConfig(domain);
  console.log(domainConfig?.['algorithm:random']?.length);
  if ((domainConfig?.['algorithm:random']?.min_length||0) > (domainConfig?.['algorithm:random']?.max_length||Infinity)) {
    res.status(500).json({
      "error": "internal-error",
      "message": "The server is not configured correctly. Please contact the administrator.",
      "technical": "The minimum length is greater than the maximum length."
    });
    return;
  }
  var shortcode = "";
  if (req.body.shortcode) {
    if (typeof req.body.shortcode !== "string") {
      res.status(400).json({
        "error": "invalid",
        "message": "The shortcode parameter is required to be a string."
      });
      return;
    }
    if ((req.body.shortcode as string).length < (domainConfig?.['algorithm:random']?.min_length||1)) {
      res.status(400).json({
        "error": "invalid",
        "message": "The shortcode parameter is required to be at least " + (domainConfig?.['algorithm:random']?.min_length||1) + " character(s) long."
      });
      return;
    }
    if ((req.body.shortcode as string).length > (domainConfig?.['algorithm:random']?.max_length||256)) {
      res.status(400).json({
        "error": "invalid",
        "message": "The shortcode parameter cannot be more than " + (domainConfig?.['algorithm:random']?.max_length||256) + " character(s) long."
      });
      return;
    }
    if (await new Shortlinked({}).findOne({shortcode: req.body.shortcode, domain})) {
      res.status(409).json({
        "error": "exists",
        "message": "The shortcode you requested already exists on the domain."
      });
      return;
    }
    shortcode = req.body.shortcode;
  }
  function regenerateShortcode() {
    shortcode = randomShortener(
      // What all this does is:
      // * Keeps the length within the bounds of the config.
      // * Forces it to generate SOMETHING, even if the config isn't quite right.
      // * Sets sane defaults if the config is not set. There's no reason you'd need an ID longer than 256 characters.
      Math.min(
        Math.max(
          req.body.length||domainConfig?.['algorithm:random']?.length||5,
          Math.max(domainConfig?.['algorithm:random']?.min_length||1, 1)
        )||5,
        (domainConfig?.['algorithm:random']?.max_length||256)
      )
    );
  }
  if (!shortcode && (domainConfig?.algorithm == "random" || !domainConfig?.algorithm)) {
    regenerateShortcode();
    var count = 0;
    while (await new Shortlinked({}).findOne({shortcode, domain})) {
      if (count > 10) {
        res.status(500).json({
          "error": "internal-error",
          "message": "Could not generate a shortcode. Please try again later.",
          "technical": "Took more than 10 tries to find an unused code. It might help to increase the default length."
        });
        return;
      }
      regenerateShortcode();
      count++;
    }
  }
  if (!shortcode) {
    res.status(500).json({
      "error": "internal-error",
      "message": "Could not generate a shortcode. Please try again later.",
      "technical": "The shortcode generator did not return a value. This probably means that something is misconfigured."
    });
  }

  if (req.body.expires_in) {
    const durationMs = parseTimeDuration(req.body.expires_in);
    if (durationMs) {
      req.body.expires_at = new Date(Date.now() + durationMs).toISOString();
    } else {
      res.status(400).json({
        "error": "invalid",
        "message": "The expires_in parameter MUST be a valid time duration string, made up of an integer and a unit suffix."
      });
      return;
    }
  }
  if (req.body.expires_at) {
    try {
      req.body.expires_at = new Date(req.body.expires_at).toISOString();
    } catch (e) {
      res.status(400).json({
        "error": "invalid",
        "message": "The expires_at parameter MUST be a valid ISO 8601 date string."
      });
      return;
    }
    if (new Date(req.body.expires_at) < new Date(Date.now())) {
      res.status(400).json({
        "error": "invalid",
        "message": "The expires_at parameter MUST be in the future."
      });
      return;
    }
  }

  if (!req.headers['x-warp-user']) {
    // this definitely should exist by now, but just in case...
    res.status(401).json({
      "error": "unauthorized",
      "message": "You must be logged in."
    });
    return;
  }

  const newData = {
    long_url: req.body.long_url,
    shortcode: shortcode,
    domain: domainConfig?.domain || process.env.PRIMARY_DOMAIN!,
    owner: req.headers['x-warp-user'] as string,
    created_at: new Date(Date.now()),
    expires_at: req.body.expires_at ? new Date(req.body.expires_at) : undefined,
    created_by_app_name: req.headers['x-warp-client-name'] as string || undefined,
    created_by_app_id: req.headers['x-warp-client-id'] as string || undefined,
  } as ShortenedLinkType;
  const link = await new Shortlinked({}).insertOne(newData);
  if (!link) {
    res.status(500).json({
      "error": "internal-error",
      "message": "Could not save the link to the database. Please try again later.",
    });
    return;
  }

  const response: ShortenResponse = {
    link: `https://${newData.domain}/${domainConfig?.prefix||''}${shortcode}`,
    long_url: newData.long_url,
    domain: newData.domain,
    shortcode,
    // prefix: process.env.PRIMARY_DOMAIN ? domainConfigOf(process.env.PRIMARY_DOMAIN).prefix : undefined,
    prefix: domainConfig?.prefix || undefined,
    created_at: newData.created_at,
    //app: newData.created_by_app_name || undefined,
    app: undefined,
    expires_at: newData.expires_at || undefined,
  };
  res.status(200).json(response);
}

export interface ShortenResponse {
  /** The shortened link, complete.
   * Consists of URI scheme (`https://`), domain, prefix if set, and the shortcode.
   *
   * ```text
   * Value: https://blakes.dev/!cr39dV
   *
   * https://  blakes.dev  /  !       cr39dV
   * Scheme    Domain         Prefix  Shortcode
   * ```
  */
  link: string;
  /** The original link. */
  long_url: string;
  /** The domain of the shortened link.
   * This is the domain selected in the UI.
   * @default process.env.PRIMARY_DOMAIN
  */
  domain: string;
  /** The shortcode of the shortened link. Also known as the slug. */
  shortcode?: string;
  /** The prefix applied to the shortened link, if there is one.
   * This may be required to access the link, but is not required
   * when using the API, including /api/v1/warp.
  */
  prefix?: string;
  /** The date and time the link was created. */
  created_at?: Date;
  /** The name of the app that created the link, if there is one. */
  app?: string;
  expires_at?: Date;
}