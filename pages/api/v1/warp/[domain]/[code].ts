import { connectToDatabase } from "@/src/database";
import { getDomainConfig } from "@/src/kdl";
import Shortlinked from "@/src/models/api/shortlinked";
import mongoose from "mongoose";
import { NextApiRequest, NextApiResponse } from "next";

// This will do a database lookup and redirect to a page, or 404.
export default async function handler(req: NextApiRequest, res: NextApiResponse) {
  //res.status(404).send("Not implemented");
  var connection: typeof mongoose;
  try {
    connection = await connectToDatabase();
  } catch (e) {
    res.status(500).json({
      "error": "internal-error",
      "message": "Could not connect to the database. Please try again later.",
      "technical": e.message
    });
    return;
  }
  const domain = (req.query.domain == 'default' ? (req.headers.host || process.env.PRIMARY_DOMAIN) : req.query.domain as string) || process.env.PRIMARY_DOMAIN;
  const domainConfig = await getDomainConfig(domain!);
  const shortcode = // Discard a set prefix if it exists.
    domainConfig?.prefix && (req.query.code as string).startsWith(domainConfig?.prefix || "\x00")
      ? (req.query.code as String).replace(
          domainConfig?.prefix || /\A(?!x)x/, ""
        )
      : (req.query.code as string);
  const link = await new Shortlinked({}).findOne({shortcode, domain: domain as string});
  const isExpired = link && link.expires_at && new Date(link.expires_at) < new Date();
  if (!link || isExpired) {
    const _accepts = req.headers.accept ? acceptsHeaderList(req.headers.accept) : [];
    if (isExpired) {
      // Delete the link if it's expired.
      await new Shortlinked({}).deleteOne({shortcode, domain: domain as string});
    }
    if (req.headers.accept && _accepts[0] == 'text/html') {
      // Likely running in a browser. Show the 404 page.
      // This does an odd hack that relies on Next.js's fetch implementation,
      // and Vercel/Next's caching implementation.
      // It looks up the 404 page that the domain probably uses
      // by using an address that is known to not exist.
      const notfoundpage = await fetch(`http://${req.headers.host}/api/404`, {cache: 'force-cache'}).then((res) => res.text());
      res.status(404).send(notfoundpage);
    } else {
      res.status(404).json({"error": "not-found"});
    }
    return;
  }
  if (link.expires_at) {
    const expiresIn = Math.floor((new Date(link.expires_at).getTime() - new Date().getTime()) / 1000);
    res.appendHeader('Cache-Control', `public, max-age=${Math.min(31536000, expiresIn)}, immutable`);
  } else {
    res.appendHeader('Cache-Control', 'public, max-age=31536000, immutable');
  }
  res.redirect(link.long_url).end();
  return;
}

function acceptsHeaderList(accepts: string) {
  return accepts.split(',').map((accept) => {
    return accept.trim().toLowerCase();
  });
}