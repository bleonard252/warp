import {  ShortenedLinkType } from "@/src/models/shortlinked";
import { NextApiRequest, NextApiResponse } from "next";
import { ShortenResponse } from "../../shorten";
import AuthPreflight from "@/src/preflight/auth";
import Shortlinked from "@/src/models/api/shortlinked";
import { getDomainConfig } from "@/src/kdl";

export default async function handler(req: NextApiRequest, res: NextApiResponse) {
  //res.status(404).send("Not implemented");
  if (!(req.method == 'GET' || req.method == 'DELETE' || req.method == 'PUT')) { // supported methods
    res.status(405).json({
      "error": "invalid",
      "message": "This endpoint only supports GET, DELETE, and PUT requests."
    });
    return;
  }
  {const preflight = await AuthPreflight(req, res); if (preflight == false) return;}
  const link = await new Shortlinked({}).findOne({shortcode: req.query.code as string, domain: req.query.domain == 'default' ? process.env.PRIMARY_DOMAIN : req.query.domain as string});
  const isExpired = link?.expires_at ? link.expires_at < new Date(Date.now()) : false;
  if (!link || isExpired) {
    if (isExpired) {
      await new Shortlinked({}).deleteOne({shortcode: req.query.code as string, domain: req.query.domain == 'default' ? process.env.PRIMARY_DOMAIN : req.query.domain as string});
    }
    res.status(404).json({"error": "not-found"});
    return;
  }
  const domain = link.domain;
  const domainConfig = await getDomainConfig(domain);
  if (link.owner != 'app' && link.owner != req.headers['x-warp-user']) {
    // Why not return 404 here instead?
    // It doesn't matter because all shortlinks are publicly accessible anyway.
    res.status(403).json({
      "error": "forbidden",
      "message": req.method == 'DELETE' ? "You are not allowed to delete this link."
      : (req.method == 'PUT' /*|| req.method == 'PATCH'*/) ? "You are not allowed to edit this link."
      : "You are not allowed to view details about this link."
    });
    return;
  }
  if (req.method == 'DELETE') {
    const { deletedCount } = await new Shortlinked({}).deleteOne({shortcode: req.query.code as string, domain: req.query.domain == 'default' ? process.env.PRIMARY_DOMAIN : req.query.domain as string});
    if (deletedCount == 0) {
      res.status(404).json({
        "error": "not-found",
        "message": "The link you are trying to delete does not exist."
      });
      return;
    }
    res.status(204).end();
    return;
  } else if (req.method == 'PUT') {
    if (!req.body.long_url) {
      res.status(400).json({
        "error": "missing-required",
        "message": "The long_url field is required."
      });
      return;
    }
    if (req.body.shortcode && req.body.shortcode != req.query.code) {
      if (await new Shortlinked({}).findOne({shortcode: req.body.shortcode, domain})) {
        res.status(409).json({
          "error": "exists",
          "message": "The shortcode you requested already exists on the domain."
        });
        return;
      }
    }
    const newData = {
      long_url: req.body.long_url,
      shortcode: req.body.shortcode || req.query.code as string,
      expires_at: req.body.expires_at || undefined,
    } as ShortenedLinkType;
    const { modifiedCount, matchedCount } = await new Shortlinked({}).updateOne({
      shortcode: req.query.code as string,
      domain: req.query.domain == 'default' ? process.env.PRIMARY_DOMAIN : req.query.domain as string
    } as unknown as Partial<ShortenedLinkType>, newData);
    if (matchedCount == 0) {
      res.status(404).json({
        "error": "not-found",
        "message": "The link you are trying to update does not exist."
      });
      return;
    }
    if (modifiedCount == 0) {
      res.status(500).json({
        "error": "not-found",
        "message": "You may be trying to update the link with the same data. The link was not modified."
      });
      return;
    }
    res.json({
      "link": `https://${link.domain}/${domainConfig?.prefix||''}${newData.shortcode}`, // TODO: apply prefix + domain
      "shortcode": newData.shortcode,
      "long_url": newData.long_url,
      "domain": link.domain, // TODO: apply domain
      "prefix": domainConfig?.prefix||undefined,
      "created_at": link.created_at,
      "app": link.created_by_app_name ?? undefined,
      "expires_at": link.expires_at ?? undefined
    } as ShortenResponse);
  } else {
    //res.appendHeader('Cache-Control', 'public, max-age=31536000, immutable');
    res.json({
      "link": `https://${link.domain}/${domainConfig?.prefix||''}${link.shortcode}`, // TODO: apply prefix
      "shortcode": link.shortcode,
      "long_url": link.long_url,
      "domain": link.domain,
      "prefix": domainConfig?.prefix||undefined,
      "created_at": link.created_at,
      "app": link.created_by_app_name ?? undefined,
      "expires_at": link.expires_at ?? undefined
    } as ShortenResponse);
  }
}