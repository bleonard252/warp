import { NextApiRequest, NextApiResponse } from "next";
import AuthPreflight from "@/src/preflight/auth";
import ApiKey from "@/src/models/api/apikey";
import { ApiKeyResponse } from "./apiKey";

export default async function handler(req: NextApiRequest, res: NextApiResponse) {
  if (req.method != 'GET') {
    res.status(405).json({
      "error": "invalid",
      "message": "This endpoint only supports GET requests."
    });
    return;
  }
  {const preflight = await AuthPreflight(req, res); if (preflight == false) return;}
  const keys = await new ApiKey().find({owner: req.headers['x-warp-user'] as string});
  res.json(keys.map(key => ({
    name: key.name,
    client_id: key.client_id,
    created_at: key.created_at,
  } as ApiKeyResponse) || []));
}