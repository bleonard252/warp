import { NextApiRequest, NextApiResponse } from "next";
import AuthPreflight from "@/src/preflight/auth";
import ApiKey from "@/src/models/api/apikey";

export default async function handler(req: NextApiRequest, res: NextApiResponse) {
  if (!(req.method == 'GET' || req.method == 'DELETE' || req.method == 'PUT')) { // supported methods
    res.status(405).json({
      "error": "invalid",
      "message": "This endpoint only supports GET, DELETE, and PUT requests."
    });
    return;
  }
  {const preflight = await AuthPreflight(req, res); if (preflight == false) return;}
  if (!req.query.id) {
    res.status(400).json({
      "error": "invalid",
      "message": "You need to specify a client ID."
    });
    return;
  }

  const key = await new ApiKey().findOne({client_id: req.query.id as string, owner: req.headers['x-warp-user'] as string});
  if (!key) {
    res.status(404).json({
      "error": "not-found",
      "message": "The specified API key was not found, or you don't have permission to access it."
    });
    return;
  }
  if (req.method == 'GET') {
    res.status(200).json({
      "client_id": key.client_id,
      "name": key.name,
      "created_at": key.created_at,
    });
    return;
  } else if (req.method == 'DELETE') {
    const { deletedCount } = await new ApiKey().deleteOne({client_id: req.query.id as string, owner: req.headers['x-warp-user'] as string});
    if (deletedCount == 0) {
      res.status(404).json({
        "error": "not-found",
        "message": "The specified API key was not found, or you don't have permission to access it."
      });
      return;
    }
    res.status(204).end();
    return;
  } else if (req.method == 'PUT') {
    const { modifiedCount, matchedCount } = await new ApiKey().updateOne({client_id: req.query.id as string, owner: req.headers['x-warp-user'] as string}, req.body);
    if (matchedCount == 0) {
      res.status(404).json({
        "error": "not-found",
        "message": "The specified API key was not found, or you don't have permission to access it."
      });
      return;
    }
    if (modifiedCount == 0) {
      res.status(400).json({
        "error": "invalid",
        "message": "You didn't change anything."
      });
      return;
    }
    res.status(204).end();
    return;
  } else {
    res.status(405).json({
      "error": "invalid",
      "message": "This endpoint only supports GET, DELETE, and PUT requests."
    });
    return;
  }
}