import { getKdlConfig } from "@/src/kdl";
import AuthPreflight from "@/src/preflight/auth";
import { NextApiRequest, NextApiResponse } from "next";

export default async function handler(req: NextApiRequest, res: NextApiResponse) {
  if (req.method != 'GET') {
    res.status(405).json({
      "error": "invalid",
      "message": "This endpoint only supports GET, DELETE, and PUT requests."
    });
    return;
  }
  {const preflight = await AuthPreflight(req, res); if (preflight == false) return;}
  const config = await getKdlConfig();
  const domainNodes = config.filter(node => node.name.toLowerCase() === "domain" && node.values.length >= 1);
  if (!domainNodes) {
    res.status(200).json({
      domains: process.env.DOMAINS?.split(",") || [process.env.PRIMARY_DOMAIN],
      default: process.env.PRIMARY_DOMAIN,
      config: new Map((process.env.DOMAINS?.split(",") || [process.env.PRIMARY_DOMAIN]).map(domain => [domain, {}])),
    });
    return;
  }
  res.status(200).json({
    domains: domainNodes.map(node => node.values[0]) || [process.env.PRIMARY_DOMAIN],
    default: domainNodes.find(node => node.properties?.default?.valueOf() === true)?.values[0] || process.env.PRIMARY_DOMAIN,
    config: new Map(domainNodes.map(node => [node.values[0], {
      prefix: node.properties?.prefix?.valueOf() || undefined,
    }])),
  });
}

export async function getDomainList() {
  const config = await getKdlConfig();
  const domainNodes = config.filter(node => node.name.toLowerCase() === "domain" && node.values.length >= 1);
  return domainNodes.map(node => node.values[0]) || [process.env.PRIMARY_DOMAIN];
}