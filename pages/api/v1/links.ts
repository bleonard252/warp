import Shortlinked from "@/src/models/api/shortlinked";
import { ShortenedLinkType } from "@/src/models/shortlinked";
import { NextApiRequest, NextApiResponse } from "next";
import { ShortenResponse } from "./shorten";
import { getDomainConfig } from "@/src/kdl";
import AuthPreflight from "@/src/preflight/auth";
import { getDomainList } from "./domains";

export default async function handler(req: NextApiRequest, res: NextApiResponse) {
  if (req.method != 'GET') {
    res.status(405).json({
      "error": "invalid",
      "message": "This endpoint only supports GET requests."
    });
    return;
  }
  {const preflight = await AuthPreflight(req, res); if (preflight == false) return;}

  req.query = req.query || {};
  const validDomains = await getDomainList();
  const filterDomain = req.query?.domain || undefined;
  if (filterDomain && !validDomains.map((v) => (v?.valueOf() as string).toLowerCase()).includes((filterDomain?.toString() ||'').toLowerCase())) {
    res.status(400).json({
      "error": "invalid",
      "message": "You must choose a valid domain to filter to."
    });
    return;
  }
  const filters = new Map();
  if (filterDomain) filters.set('domain', filterDomain);

  await new Shortlinked({}).deleteMany({expires_at: {$lt: new Date(Date.now())} as any}); // purge expired links
  const links = await new Shortlinked({}).find({$or: [ {owner: req.headers['x-warp-user']}, {owner: 'app'} ], ...filters} as unknown as Partial<ShortenedLinkType>);
  res.json(await Promise.all(links.map(async link => {
    const domainConfig = await getDomainConfig(link.domain);
    return {
      "link": `https://${link.domain}/${domainConfig?.prefix||''}${link.shortcode}`, // TODO: apply prefix
      "shortcode": link.shortcode,
      "long_url": link.long_url,
      "domain": link.domain,
      "prefix": domainConfig?.prefix||undefined,
      "created_at": link.created_at,
      "app": link.created_by_app_name ?? undefined,
      "expires_at": link.expires_at ?? undefined,
    } as ShortenResponse;
  }) || []));
}