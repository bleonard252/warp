import assert from 'assert';
import type { NextApiRequest, NextApiResponse } from 'next'

import CryptoES from 'crypto-es';

import { Issuer, TokenSet } from 'openid-client';
import { setcookieLaxSecurityFeatures, setcookieSecurityFeatures } from '@/src/cookies';

function readCookiesFromHeader(headerValue: string): Map<string, string> {
  return new Map<string, string>(headerValue.split(/; ?/).map((cookie) => {
    const [k,v] = cookie.split('=');
    if (Array.isArray(v)) {
      return [k.trim(), v.join(', ')];
    }
    if (k && !v) {
      return [k.trim(), 'undefined'];
    } else if (!k) {
      return ['undefined', 'undefined'];
    }
    return [k?.trim() || 'undefined', v?.trim() || 'undefined'];
  }));
}

export default async function handler(req: NextApiRequest, res: NextApiResponse) {
  assert(process.env.OPENID_ROOT, "OPENID_ROOT is not set");
  assert(process.env.OPENID_CLIENT_ID, "OPENID_CLIENT_ID is not set");
  const cookies = readCookiesFromHeader(req.headers.cookie || "");
  const challengeCookie = cookies.get("warp_oauth_challenge") as string | undefined;
  if (!challengeCookie && !cookies.get("warp_oauth_noretry")) {
    res.appendHeader("Refresh", "5; url=/api/v1/auth/login?noretry=1")
    res.status(400).json({
      "error": "expired",
      "message": "Missing challenge cookie. If you are in a browser, you will be redirected to try again."
    });
    return;
  } else if (!challengeCookie) {
    res.appendHeader("Set-Cookie", `warp_oauth_noretry=; Max-Age=0; SameSite=strict; HttpOnly; SameSite=Strict;`);
    res.status(400).json({
      "error": "expired",
      "message": "Missing challenge cookie. Please try again.",
      "noretry_message": "We've already automatically retried and it brought us back here. This shouldn't happen."
    });
    return;
  }
  const redirectCookie = req.cookies["warp_auth_redirect"];
  const issuer = await Issuer.discover(process.env.OPENID_ROOT);
  if (!issuer) {
    return res.status(500).json({
      "error": "discovery-failed",
      "message": 'Could not discover OpenID configuration. The auth server may be down.'
    });
  }
  if (!process.env.RANDOM_SECRET) {
    console.warn("No RANDOM_SECRET set! This is a security risk. Please set it in your .env file.");
  }
  var decryptedChallenge: string = challengeCookie;
  try {
    //decryptedChallenge = process.env.RANDOM_SECRET ? CryptoES.AES.decrypt(challengeCookie, process.env.RANDOM_SECRET).toString(CryptoES.enc.Base64) : challengeCookie;
  } catch (e) {
    console.error(e);
    return res.status(500).json({
      "error": "failed",
      "technical": e.toString(),
      "message": 'Could not decrypt challenge cookie. This is a developer error and should not have happened.',
    });
  }
  const client = new issuer.Client({
    redirect_uris: [`https://${process.env.PRIMARY_DOMAIN}/api/v1/auth/callback`],
    client_id: process.env.OPENID_CLIENT_ID,
    client_secret: process.env.OPENID_CLIENT_SECRET,
    response_types: ['code'],
  });
  const callbackParams = client.callbackParams(req);
  if (callbackParams.state === "") {
    callbackParams.state = undefined;
  }
  var callbackResponse: TokenSet;
  try {
    callbackResponse = await client.callback(`https://${process.env.PRIMARY_DOMAIN}/api/v1/auth/callback`, callbackParams, { code_verifier: decryptedChallenge });
    if (callbackResponse.expired()) {
      res.appendHeader("Refresh", "5; url=/api/v1/auth/login")
      return res.status(401).json({"error": "expired", "message": "Authorization expired. If you are in a browser, you will be redirected to try again."});
    }
  } catch (e) {
    console.error(e);
    return res.status(401).json({"error": "unauthorized"});
  }
  if (process.env.OPENID_CLAIM && !callbackResponse.claims()[process.env.OPENID_CLAIM]) {
    return res.status(401).json({"error": "unauthorized", "message": `You do not have the ${process.env.OPENID_CLAIM} claim.`});
  }
  // Set the warp_credential cookie to the access token
  res.appendHeader("Set-Cookie", `warp_credential=${callbackResponse.access_token}; ${setcookieSecurityFeatures()}Max-Age=${callbackResponse.expires_in}; Path=/;`);
  // Unset these cookies; they're no longer needed
  res.appendHeader("Set-Cookie", `warp_oauth_challenge=; ${setcookieLaxSecurityFeatures()}Max-Age=0;`);
  res.appendHeader("Set-Cookie", `warp_auth_redirect=; ${setcookieLaxSecurityFeatures()}Max-Age=0;`);
  res.redirect(307, redirectCookie || '/admin');
}
