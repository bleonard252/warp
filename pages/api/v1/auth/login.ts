import { setcookieLaxSecurityFeatures, setcookieSecurityFeatures } from '@/src/cookies';
import assert from 'assert';
import CryptoES from 'crypto-es';
import type { NextApiRequest, NextApiResponse } from 'next'
import { Issuer } from 'openid-client';
import { generators } from 'openid-client';

export default async function handler(req: NextApiRequest, res: NextApiResponse) {
  assert(process.env.OPENID_ROOT, "OPENID_ROOT is not set in .env");
  assert(process.env.OPENID_CLIENT_ID, "OPENID_CLIENT_ID is not set in .env");
  const code_verifier = generators.codeVerifier();
  const issuer = await Issuer.discover(process.env.OPENID_ROOT);
  const code_challenge = generators.codeChallenge(code_verifier);
  const client = new issuer.Client({
    redirect_uris: [`https://${process.env.PRIMARY_DOMAIN}/api/v1/auth/callback`],
    client_id: process.env.OPENID_CLIENT_ID,
    client_secret: process.env.OPENID_CLIENT_SECRET,
    response_types: ['code'],
  });
  if (!process.env.RANDOM_SECRET) {
    console.warn("No RANDOM_SECRET set! This is a security risk. Please set it in your .env file.");
  }
  //const encrypted_verifier = process.env.RANDOM_SECRET ? CryptoES.AES.encrypt(Buffer.from(code_verifier, 'base64url').toString(), process.env.RANDOM_SECRET).toString(CryptoES.format.Hex) : code_verifier;
  const encrypted_verifier = code_verifier;
  res.appendHeader("Set-Cookie", `warp_oauth_challenge=${encrypted_verifier}; ${setcookieLaxSecurityFeatures()}Max-Age=300;`);
  if (req.query.noretry === '1') {
    res.appendHeader("Set-Cookie", `warp_oauth_noretry=1; ${setcookieLaxSecurityFeatures()}Max-Age=300;`);
  }
  if (req.query.redirect) {
    res.appendHeader("Set-Cookie", `warp_auth_redirect=${req.query.redirect}; ${setcookieLaxSecurityFeatures()}Max-Age=300;`);
  }
  res.redirect(307, client.authorizationUrl({
    scope: 'openid profile email',
    code_challenge,
    code_challenge_method: 'S256',
    redirect_uri: `https://${process.env.PRIMARY_DOMAIN}/api/v1/auth/callback`,
    response_type: 'code',
  }));
}