import AuthPreflight from '@/src/preflight/auth';
import type { NextApiRequest, NextApiResponse } from 'next'

export default async function handler(req: NextApiRequest, res: NextApiResponse) {
  {const preflight = await AuthPreflight(req, res); if (preflight == false) return;}
  res.status(200).send(`<!DOCTYPE html>
  <html>
    <head>
      <title>Success!</title>
      <meta name="viewport" content="width=device-width, initial-scale=1">
    </head>
    <body>
      <h1>Success!</h1>
      <p>You have successfully logged in. You may now close this window.</p>
    </body>
  </html>`);
}