import { setcookieLaxSecurityFeatures } from '@/src/cookies';
import assert from 'assert';
import type { NextApiRequest, NextApiResponse } from 'next'
import { Issuer } from 'openid-client';
import { generators } from 'openid-client';

export default async function handler(req: NextApiRequest, res: NextApiResponse) {
  assert(process.env.OPENID_ROOT, "OPENID_ROOT is not set in .env");
  assert(process.env.OPENID_CLIENT_ID, "OPENID_CLIENT_ID is not set in .env");
  const issuer = await Issuer.discover(process.env.OPENID_ROOT);
  const client = new issuer.Client({
    redirect_uris: [`https://${process.env.PRIMARY_DOMAIN}/api/v1/auth/callback`],
    client_id: process.env.OPENID_CLIENT_ID,
    client_secret: process.env.OPENID_CLIENT_SECRET,
    response_types: ['code'],
  });
  if (!process.env.RANDOM_SECRET) {
    console.warn("No RANDOM_SECRET set! This is a security risk. Please set it in your .env file.");
  }
  res.appendHeader("Set-Cookie", `warp_credential=; ${setcookieLaxSecurityFeatures()}Max-Age=0;Path=/;`);
  res.redirect(307, client.endSessionUrl({
    client_id: process.env.OPENID_CLIENT_ID,
    post_logout_redirect_uri: `https://${process.env.PRIMARY_DOMAIN}/`,
    id_token_hint: req.cookies.warp_credential,
  }));
}