import React, { AriaAttributes, Component, ReactNode, Ref } from "react";

const baseClasses = "rounded-md p-2 inline-block items-center transition-colors";
const flexClasses = " flex flex-row gap-2";
const _primaryFilledClasses = " bg-primary-3 hover:bg-primary-5 text-onprimary-3 enabled:active:bg-primary-2 disabled:bg-primary-2/50 disabled:text-onprimary-2/50";
const _primaryUnfilledClasses = " text-primary-4 dark:text-primary-3 enabled:hover:text-primary-2 enabled:hover:bg-primary-2/25 enabled:active:bg-primary-2/10 disabled:text-primary-2/50";
const _secondaryUnfilledClasses = " bg-transparent text-onscheme-2 enabled:hover:bg-onscheme-2/25 enabled:active:bg-onscheme-2/10 disabled:text-onscheme-2/50";

export function Button(props: {
  children?: ReactNode,
  className?: string,
  flex?: boolean,
  primary?: boolean,
  filled?: boolean,
  static?: boolean,
} & Record<string,any>) {
  var primaryFilledClasses = _primaryFilledClasses;
  var primaryUnfilledClasses = _primaryUnfilledClasses;
  var secondaryUnfilledClasses = _secondaryUnfilledClasses;

  if (props.static) {
    //console.log("This is static.");
    primaryFilledClasses = primaryFilledClasses.replace(/(?:^|\s)(?:enabled:|disabled)?(?:hover|active)\:\S*/g, "");
    primaryUnfilledClasses = primaryUnfilledClasses.replace(/(?:^|\s)(?:enabled:|disabled)?(?:hover|active)\:\S*/g, "");
    secondaryUnfilledClasses = secondaryUnfilledClasses.replace(/(?:^|\s)(?:enabled:|disabled)?(?:hover|active)\:\S*/g, "");
  }

  return React.createElement(props.href ? 'a' : 'button', {
    ...props,
    flex: undefined,
    primary: undefined,
    filled: undefined,
    static: undefined,
    role: "button",
    className: baseClasses + (props.flex ? flexClasses : "") + (props.primary ? props.filled ? primaryFilledClasses : primaryUnfilledClasses : secondaryUnfilledClasses)+" "+(props.className??""),
  }, props.children);
}