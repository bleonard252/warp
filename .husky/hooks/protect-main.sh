#!/usr/bin/env bash

branch=$(git symbolic-ref HEAD | sed -e 's,.*/\(.*\),\1,')

# Prevent commiting to main
if [ "$branch" = "main" ]; then
  echo "You cannot commit directly to main. If you're totally sure this is what you want to do, use --no-verify."
  exit 1
fi