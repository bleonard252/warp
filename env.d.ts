declare namespace NodeJS {
  interface ProcessEnv {
    /** The MongoDB connection string. */
    readonly MONGODB_URI: string | undefined,
    /** The base URI for the MongoDB Data API.
     * This should be preferred over MONGODB_URI if set.
     */
    readonly MONGO_DATA_API_BASE: string | undefined,
    /** The cluster to use for the MongoDB Data API.
     * This is the "database deployment" in MongoDB Atlas.
     */
    readonly MONGO_DATA_API_CLUSTER: string | undefined,
    /** The API key to use for the MongoDB Data API. */
    readonly MONGO_DATA_API_KEY: string | undefined,
    /** The root under which to discover the OpenID configuration.
        It probably looks like this:
        ```plain
        https://keycloak.localhost.test/realm/REALMNAME/
        ``` */
    readonly OPENID_ROOT: string | undefined,
    /** The client ID for the OpenID client. */
    readonly OPENID_CLIENT_ID: string | undefined,
    /** The client secret for the OpenID client. */
    readonly OPENID_CLIENT_SECRET: string | undefined,
    /** The domain name that the admin panel is hosted on.
        This is assumed to be HTTPS. **DO NOT** include the protocol. */
    readonly PRIMARY_DOMAIN: string | undefined,
    /** Domains that are used for shortened links. */
    readonly DOMAINS: string | undefined,
    /** A random secret code to encrypt things with. */
    readonly RANDOM_SECRET: string | undefined,
    /** The claim to check for in the OpenID response. If not set, don't check for it. */
    readonly OPENID_CLAIM: string | undefined,
  }
}