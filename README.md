# Source code for warp.blakes.dev
This is the source code for my personal link shortener. You're free to deploy your own copy.

## Prerequisites
You'll need:
* A domain, preferably short and speakable.
* A reverse proxy (if you want to use a prefixed domain, like the way blakes.dev is set up). I recommend Caddy because it's what I use, but you should be able to pull it off with nginx or httpd if you so choose.
* A MongoDB database. If you choose to use Atlas, you should consider the serverless and/or Data API options.
* A cache (most Node.js hosting offers this transparently).
* `pnpm` wherever you choose to host it. Probably not necessary but it might break something if you don't.
* Somewhere to host it. You can just deploy it with plain Node.js if you have a VPS, Coolify, Heroku, or something similar; see below. It's built for Vercel so it should work out of the box with that.

## Configuration
1. Create a `config.kdl` file next to the README, and put in any configuration according to the example of `config.kdl.example`. This file has to exist, even if you are setting the config in the `KDL_CONFIG` environment variable (see `.env.example`).
2. Create a `.env` file next to the README, and add in all of the applicable values according to `.env.example`.

## Deploy with Vercel
On a computer or an Android phone with Termux installed (you only need this to set it up), and assuming you already have an account:
1. Clone the repository and switch into it: `git clone https://source.blakes.dev/me/warp warp-shortener && cd warp-shortener`
2. Get dependencies for build time. `pnpm i`
3. Set up configuration files (see Configuration above).
4. Run the build. `pnpm build`
5. Set up environment variables. `pnpx vercel env push` (Note pnp**x** here instead of pnp**m**!)
6. Link and deploy. `pnpx vercel --prod`

To update:
1. Enter your clone: `cd warp-shortener`
2. Pull the changes with `git pull` OR switch to a specific version with `git checkout VERSION`.
3. If you haven't changed configuration, perform steps 2, 4, and 6 from above again. Otherwise, perform step 2, and steps 4-6 again.

## Deploy on a VPS
On a VPS:
1. Clone the repository and switch into it: `git clone https://source.blakes.dev/me/warp warp-shortener && cd warp-shortener`
2. Get dependencies for build time. `pnpm i`
3. Set up configuration files (see Configuration above).
4. Run the build. `pnpm build`
5. Deploy. `pnpm start`

To update:
1. Make sure the server is not running, then switch into the directory. `cd warp-shortener`
2. Pull the changes with `git pull` OR switch to a specific version with `git checkout VERSION`.
3. If you haven't changed configuration, perform steps 2, 4, and 5 from above again. Otherwise, perform steps 2-5 again.

## Deploy on a Heroku or Coolify like site
These instructions are generic. You can ask for help but you're kinda on your own here.
1. Upload [the source code](https://source.blakes.dev/me/warp/archive/main.zip) to the site. **Don't** use Git (or GitHub etc) to do it.
2. Set the shortener to use Next.js settings. This includes `pnpm i` as the "install dependencies" command, `pnpm build` as the "build" command, and `pnpm start` as the "run" command.
3. Configure your environment variables, including `KDL_CONFIG` if you want optional config.
4. Deploy.

To update:
1. Re-upload the source code.
2. Make sure your configs are up to date.
3. Deploy.

## License
warp.blakes.dev Link Shortener
Copyright (C) 2023 Blake Leonard

This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License along with this program. If not, see https://www.gnu.org/licenses/.