module.exports = {
  rules: {
    "header-max-length": [1, "always", 50],
    "body-max-line-length": [1, "always", 72],
    "type-case": [2, "always", "lower-case"],
    "type-enum": [2, "always", ["feat", "fix", "docs", "style", "refactor", "test", "chore", "revert", "release"]],
    "scope-case": [0, "always", "pascal-case"],
    "scope-enum": [1, "always", ["Shorten", "Keys", "Auth", "Admin"]],
    "scope-empty": [0, "never"],
    "body-case": [1, "always", "sentence-case"],
    "signed-off-by": [1, "always", "Signed-off-by:"],
    "subject-case": [1, "never", "lower-case"],
    "subject-full-stop": [2, "never", "."],
    "subject-exclamation-mark": [1, "never", "!"],
  }
}