// ShortenedLink schema
import mongoose from 'mongoose';

export const ShortenedLinkSchema = new mongoose.Schema({
  long_url: {
    type: String,
    required: true,
  },
  shortcode: {
    type: String,
    required: true,
  },
  owner: {
    type: String,
    required: true,
  },
  created_by_app_id: {
    type: String,
    required: false,
  },
  created_by_app_name: {
    type: String,
    required: false,
  },
  domain: {
    type: String,
    required: true,
  },
  created_at: {
    type: Date,
    required: true,
  },
  expires_at: {
    type: Date,
    required: false,
  },
});

export interface ShortenedLinkType {
  long_url: string,
  shortcode: string,
  owner: string,
  created_by_app_id?: string,
  created_by_app_name?: string,
  domain: string,
  created_at: Date,
  expires_at?: Date,
}
export type ShortenedLinkDocument = (mongoose.Document<unknown, {}, ShortenedLinkType> & Omit<ShortenedLinkType & {
  id: mongoose.Schema.Types.ObjectId;
}, never>);

const ShortenedLink = mongoose.models['ShortenedLink']
  ? mongoose.model('ShortenedLink')
  : mongoose.model('ShortenedLink', ShortenedLinkSchema, undefined, {overwriteModels: false});

export default ShortenedLink;