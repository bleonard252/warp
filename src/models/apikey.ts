// ShortenedLink schema
import mongoose from 'mongoose';

export const ApiKeySchema = new mongoose.Schema({
  owner: {
    type: String,
    required: true,
  },
  name: {
    type: String,
    required: true,
  },
  client_id: {
    type: String,
    required: true,
  },
  token: {
    type: String,
    required: true,
  },
  created_at: {
    type: Date,
    required: true,
  },
});

export interface ApiKeyType {
  owner: string;
  name: string;
  client_id: string;
  token: string;
  created_at: Date;
}
export type ApiKeyDocument = (mongoose.Document<unknown, {}, ApiKeyType> & Omit<ApiKeyType & {
  id: mongoose.Schema.Types.ObjectId;
}, never>);

const ApiKeyGlobalModel = mongoose.models['ApiKey']
  ? mongoose.model('ApiKey')
  : mongoose.model('ApiKey', ApiKeySchema, undefined, {overwriteModels: false});

export default ApiKeyGlobalModel;