import mongoose from "mongoose";

declare abstract class APITypeBase<B> {
  static const type: string;

  /** Find a single document. */
  findOne(filter: Partial<B>, options?: Omit<findOptions, 'filter'>): Promise<B?>;

  /** Find multiple documents. */
  find(filter: Partial<B>, options?: Omit<findMultipleOptions, 'filter'>): Promise<B[]>;

  /** Insert a single document.
   * @returns the ID.
  */
  insertOne(document: B, options?: Omit<insertOneOptions, 'document'>): Promise<string>;

  /** Update a single document. */
  updateOne(filter: Partial<B>, update: Partial<B>, options?: Omit<updateOptions, 'filter'>): Promise<{matchedCount: number, modifiedCount: number, upsertedId?: string}>;

  /** Delete a single document. */
  deleteOne(filter: Partial<B>, options?: Omit<deleteOneOptions, 'filter'>): Promise<{deletedCount: number}>;
}

interface findOptions {
  /** The name of the cluster. */
  dataSource: string;
  /** The name of the database. */
  database: string;
  /** The name of the collection. */
  collection: string;
  /** 
   * A [MongoDB Query Filter](https://www.mongodb.com/docs/manual/tutorial/query-documents/).
   * The `findOne` action returns the first document in the collection that matches this filter.
   *
   * If you do not specify a `filter`, the action matches all document in the collection.
   */
  filter?: mongoose.FilterQuery;
  /**
   * A [MongoDB Query Projection](https://www.mongodb.com/docs/manual/tutorial/project-fields-from-query-results/).
   * Depending on the projection, the returned document will either omit specific fields or include only specified fields or values
   */
  projection?: any;
}

interface findMultipleOptions extends findOptions {
  /**
   * A [MongoDB Sort Expression](https://www.mongodb.com/docs/manual/reference/operator/aggregation/sort/). Matched documents are returned in ascending or descending order of the fields specified in the expression.
   */
  sort?: any;
  /**
   * The maximum number of matched documents to include in the returned result set. Each request may return up to 50,000 documents.
   */
  limit?: number;
  /**
   * The number of matched documents to skip before adding matched documents to the result set.
   */
  skip?: number;
}

interface insertOneOptions {
  /** The name of the cluster. */
  dataSource: string;
  /** The name of the database. */
  database: string;
  /** The name of the collection. */
  collection: string;
  /** An [EJSON](https://www.mongodb.com/docs/manual/reference/mongodb-extended-json/) document to insert into the collection. */
  document: any;
}

interface updateOneOptions extends Omit<insertOneOptions, 'document'> {
  /**
   * A [MongoDB Update Expression](https://www.mongodb.com/docs/manual/tutorial/update-documents/) that specifies how to modify the matched document.
   */
  update: any;
  /**
   * A [MongoDB Query Filter](https://www.mongodb.com/docs/manual/tutorial/query-documents/).
   * The `updateOne` action modifies the first document in the collection that matches this filter.
   */
  filter: mongoose.FilterQuery;
  /**
   * The `upsert` flag only applies if no documents match the specified `filter`. If `true`, the `updateOne` action inserts a new document that matches the filter with the specified `update` applied to it.
   */
  upsert?: boolean;
}

type deleteOneOptions = Omit<findOptions, 'projection'>;