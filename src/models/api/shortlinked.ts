import { dbName, shouldUseDataApi } from "@/src/database";
import type { ShortenedLinkType } from "../shortlinked";
import DefaultAPIImplementation from "./default";
import ShortenedLink from "../shortlinked";

export default class Shortlinked extends DefaultAPIImplementation<ShortenedLinkType> {
  static readonly type: string = "shortenedlinks";

  constructor({cluster, database, collection}: {cluster?: string, database?: string, collection?: string}) {
    super({
      cluster: cluster || process.env.MONGO_DATA_API_CLUSTER || "default",
      database: database || dbName,
      collection: collection ?? Shortlinked.type,
      model_name: "ShortenedLink",
    });
    if (!shouldUseDataApi()) {
      ShortenedLink; // call to initialize model
    }
  }
}