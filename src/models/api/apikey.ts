import { dbName, shouldUseDataApi } from "@/src/database";
import DefaultAPIImplementation from "./default";
import ApiKeyGlobalModel, { ApiKeyType } from "../apikey";

export default class ApiKey extends DefaultAPIImplementation<ApiKeyType> {
  static readonly type: string = "apikeys";

  constructor() {
    super({
      cluster: process.env.MONGO_DATA_API_CLUSTER || "default",
      database: dbName,
      collection: ApiKey.type,
      model_name: "ApiKey"
    });
    if (!shouldUseDataApi()) {
      ApiKeyGlobalModel;
    }
  }
}