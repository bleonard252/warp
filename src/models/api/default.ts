import assert from "assert";
import type { APITypeBase, deleteOneOptions, findMultipleOptions, findOptions, insertOneOptions, updateOneOptions } from "./mongodb";
import { connectToDatabase, shouldUseDataApi } from "@/src/database";

export default class DefaultAPIImplementation<B> implements APITypeBase<B> {
  public cluster: string;
  public database: string;
  public collection: string;
  public model_name: string;

  constructor({cluster, database, collection, model_name}: {cluster?: string, database?: string, collection?: string, model_name?: string}) {
    assert(collection, "collection must be specified");
    this.cluster = cluster || process.env.MONGO_DATA_API_CLUSTER || "default";
    this.database = database || "development";
    this.model_name = model_name ?? "default";
    this.collection = collection ?? (this.model_name.toLowerCase().endsWith("s") ? this.model_name.toLowerCase() : this.model_name.toLowerCase()+"s");
  }

  async POST(endpoint: string, body: any, options?: {dataSource?: string, database?: string, collection?: string}): Promise<any> {
    return fetch(process.env.MONGO_DATA_API_BASE+"/v1/action/findOne", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        "api-key": process.env.MONGO_DATA_API_KEY || "",
      },
      body: JSON.stringify({
        dataSource: options?.dataSource || this.cluster,
        database: options?.database || this.database,
        collection: options?.collection || this.collection,
        ...body
      })
    }).then(res => {
      if (res.ok) return res;
      else {
        res.body?.getReader().read().then(value => {
          console.error(`Error ${res.status} while fetching ${process.env.MONGO_DATA_API_BASE}${endpoint}: ${value.value ? Buffer.from(value.value).toString('utf8') : res.statusText}`);
          throw res.statusText;
        });
      }
    });
  }

  findOne(filter: Partial<B>, options?: Omit<findOptions, "filter"> | undefined): Promise<B | null> {
    if (shouldUseDataApi()) {
      return this.POST("/v1/action/findOne", {
        filter,
        projection: options?.projection || undefined
      }, options).then(res => res.json().then(res => res.document || null));
    } else {
      return connectToDatabase().then(connection => connection.model<B>(this.model_name).findOne(
        filter,
        options?.projection || undefined
      ).exec().then(res => res?.toObject() || null));
    }
  }
  find(filter: Partial<B>, options?: Omit<findMultipleOptions, "filter"> | undefined): Promise<B[]> {
    if (shouldUseDataApi()) {
      return this.POST("/v1/action/find", {
        filter,
        projection: options?.projection || undefined,
        sort: options?.sort || undefined,
        limit: options?.limit || undefined,
        skip: options?.skip || undefined
      }, options).then(res => res.json().then(res => res.documents || []));
    } else {
      return connectToDatabase().then(connection => connection.model<B>(this.model_name).find(
        filter,
        options?.projection || undefined,
        {
          sort: options?.sort || undefined,
          limit: options?.limit || undefined,
          skip: options?.skip || undefined
        }
      ).exec().then(res => res.map(v => v.toObject())));
    }
  }
  insertOne(document: B, options?: Omit<insertOneOptions, "document"> | undefined): Promise<string> {
    if (shouldUseDataApi()) {
      return this.POST("/v1/action/insertOne", {
        document
      }, options).then(res => res.json().then(res => res.insertedId));
    } else {
      return connectToDatabase().then(connection => connection.model<B>(this.model_name).create(document).then(res => res.id));
    }
  }
  updateOne(filter: Partial<B>, update: Partial<B>, options?: Omit<updateOneOptions, "filter"> | undefined): Promise<{matchedCount: number, modifiedCount: number, upsertedId?: string}> {
    if (shouldUseDataApi()) {
      return this.POST("/v1/action/updateOne", {
        filter,
        update: {$set: update} as any,
        upsert: options?.upsert || undefined
      }, options).then(res => res.json().then(res => res));
    } else {
      return connectToDatabase().then(connection => connection.model<B>(this.model_name).updateOne(
        filter,
        {$set: update} as any,
        {
          upsert: options?.upsert || undefined
        }
      ).exec().then(res => ({
        ...res,
        upsertedId: res.upsertedId?.toString()
      })));
    }
  }
  deleteOne(filter: Partial<B>, options?: Omit<deleteOneOptions, "filter"> | undefined): Promise<{deletedCount: number}> {
    if (shouldUseDataApi()) {
      return this.POST("/v1/action/deleteOne", {
        filter
      }, options).then(res => res.json().then(res => res));
    } else {
      return connectToDatabase().then(connection => connection.model<B>(this.model_name).deleteOne(
        filter
      ).exec().then(res => res));
    }
  }
  deleteMany(filter: Partial<B>, options?: Omit<deleteOneOptions, "filter"> | undefined): Promise<{deletedCount: number}> {
    if (shouldUseDataApi()) {
      return this.POST("/v1/action/deleteMany", {
        filter
      }, options).then(res => res.json().then(res => res));
    } else {
      return connectToDatabase().then(connection => connection.model<B>(this.model_name).deleteMany(
        filter
      ).exec().then(res => res));
    }
  }
}