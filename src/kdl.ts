import { readFile } from "fs/promises";
import { kdljs, parse } from "kdljs";

var config: kdljs.Document|null = null;

export async function getKdlConfig(): Promise<kdljs.Document> {
  if (config) {
    // Already loaded. Return it.
    return config;
  }
  if (process.env.KDL_CONFIG) {
    const result = parse(Buffer.from(process.env.KDL_CONFIG, 'base64').toString());
    result.errors.forEach((error) => {
      console.error(error);
    });
    if (!result.output) {
      throw result.errors?.reverse()[0] || Error("Could not parse KDL config");
    }
    config = result.output;
    return result.output;
  } else {
    const file = await readFile(new URL("../config.kdl", import.meta.url)).catch(() => []);
    if (!file.length) {
      //throw Error("Could not find KDL config. Please set the KDL_CONFIG environment variable to the base64-encoded config.kdl file.");
      return [];
    }
    const result = parse(file.toString());
    result.errors.forEach((error) => {
      console.error(error);
    });
    if (!result.output) {
      throw result.errors?.reverse()[0] || Error("Could not parse KDL config");
    }
    config = result.output;
    return result.output;
  }
}

interface DomainConfig {
  /** The domain in question, as configured. */
  domain: string;
  // /** Whether this is the primary domain, according to the config.
  //  * @deprecated Use `process.env.PRIMARY_DOMAIN` instead.
  // */
  // primary: boolean;
  /** What shortcode generator to use.
   *
   * Note: this isn't checked right now, and will always be `random`.
  */
  algorithm?: 'random' | 'words';
  /** The prefix to apply to and strip from all shortcodes on this domain.
   * Don't save the prefix in the database, as it will be stripped when looking it up.
   */
  prefix?: string;
  /** If `algorithm` is `random`, what rules to apply to generating shortcodes. */
  "algorithm:random"?: {
    /** The default length to use if none is provided. */
    length?: number;
    /** The minimum length to allow when generating. */
    min_length?: number;
    /** The maximum length to allow when generating. */
    max_length?: number;
  };
}

export async function getDomainConfig(domain: string): Promise<DomainConfig|null> {
  const config = await getKdlConfig();
  const filtered = config.filter((node) => node.name.toLowerCase() == 'domain' && node.values[0]?.toString().toLowerCase() == domain.toLowerCase());
  if (!filtered.length) {
    return null;
  }
  const domainNode = filtered[0];
  function intOrUndefined(value: any): number|undefined {
    return typeof value === 'number' ? value : undefined;
  }
  const random = domainNode.properties?.algorithm?.toString() == 'random' ? {
    length: intOrUndefined(domainNode.children.find((node) => node.name == 'algorithm:random')?.properties?.length?.valueOf()),
    min_length: intOrUndefined(domainNode.children.find((node) => node.name == 'algorithm:random')?.properties?.min_length?.valueOf()),
    max_length: intOrUndefined(domainNode.children.find((node) => node.name == 'algorithm:random')?.properties?.max_length?.valueOf()),
  } : undefined;
  return {
    domain: domainNode.values[0]!.toString(),
    // primary: domainNode.attributes?.primary === true,
    algorithm: domainNode.properties?.algorithm?.toString() as 'random' | 'words' | undefined,
    prefix: typeof domainNode.properties?.prefix === 'string' && domainNode.properties?.prefix || undefined,
    "algorithm:random": random
  }
}