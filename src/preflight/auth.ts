import { NextApiRequest, NextApiResponse } from "next";
import { readCookiesFromHeaderValue } from "../cookies";
import { NextURL } from "next/dist/server/web/next-url";
import { ok } from "assert";
import { UserinfoResponse } from "openid-client";
import ApiKey from "../models/api/apikey";

const fixUrl = (url: string) => {
  const parsed = new NextURL(url);
  parsed.pathname = parsed.pathname.replace('//', '/');
  return parsed.toString();
}

export default async function AuthPreflight(req: NextApiRequest, res: NextApiResponse, redirect: boolean = false): Promise<boolean> {
  const authHeader = req.headers.authorization;
  const cookies = readCookiesFromHeaderValue(req.headers.cookie || '');
  const authCookie = cookies.get('warp_credential')

  if (authHeader) {
    var success = true;
    const authType = authHeader.split(' ')[0];
    try {
      ok(authType === 'Bearer')
    } catch(_) {
      success = false
    }
    const authValue = authHeader.split(' ')[1];

    const key = await new ApiKey().findOne({token: authValue});
    if (!key) {
      res.status(401).json({
        "error": "unauthorized",
        "message": "Invalid API key."
      });
      return false;
    }

    // TODO: check if key is expired

    if (authValue && key && success) {
      req.headers['x-warp-client-id'] = key.client_id;
      req.headers['x-warp-client-name'] = key.name;
      req.headers['x-warp-user'] = key.owner;
      return true
    }
  } else if (authCookie) {
    var oidconfig: any;
    try {
      oidconfig = await (await fetch(fixUrl(process.env.OPENID_ROOT+"/.well-known/openid-configuration"))).json();
    } catch(_) {
      res.status(500).json({
        "error": "discovery-failed",
        "message": 'Could not discover OpenID configuration. The auth server may be down.'
      });
      return false;
    }
    if (!oidconfig["userinfo_endpoint"]) {
      console.log(oidconfig);
      console.log(process.env.OPENID_ROOT+"/.well-known/openid-configuration");
      console.log(fixUrl(process.env.OPENID_ROOT+"/.well-known/openid-configuration"));
      res.status(500).json({
        "error": "discovery-failed",
        "message": 'Could not discover OpenID configuration. The auth server may be down.'
      });
      return false;
    }
    const userinfoResponse = await fetch(oidconfig["userinfo_endpoint"], {
      headers: new Headers({
        'Authorization': `Bearer ${authCookie}`,
      })
    });
    if (userinfoResponse.status == 401) {
      res.status(401).json({
        "error": "unauthorized",
        "message": 'The access token is invalid.'
      });
      return false;
    } else if (userinfoResponse.status != 200) {
      res.status(500).json({
        "error": "unknown",
        "message": 'An unknown error occurred. Please try again later.'
      });
      return false;
    }
    const userinfo: UserinfoResponse = await userinfoResponse.json();
    if (userinfo) {
      req.headers['x-warp-user'] = userinfo.sub;
      if (process.env.OPENID_CLAIM) {
        if (userinfo[process.env.OPENID_CLAIM]) {
          return true
        } else {
          res.status(401).json({
            "error": "unauthorized",
          });
          return false;
        }
      }
      return true;
    } else {
      return false;
    }
  }

  if (redirect) res.redirect('/api/v1/auth/login');
  else res.status(401).json({
    "error": "unauthorized",
  });
  return false;
}
