import CryptoES from "crypto-es";
import mongoose from "mongoose";

var connection: typeof mongoose;

var __useDataApi__internal = false;
export function shouldUseDataApi() : boolean {
  return __useDataApi__internal ??= (process.env.MONGO_DATA_API_BASE && process.env.MONGO_DATA_API_KEY && process.env.MONGO_DATA_API_CLUSTER) != null;
}

export async function connectToDatabase() {
  return connection ??= await mongoose.connect(process.env.MONGODB_URI!,{
    serverSelectionTimeoutMS: 3000,
    dbName: dbName,
  });
}

export const dbName = CryptoES.SHA256(process.env.RANDOM_SECRET || process.env.MONGODB_URI).toString(CryptoES.enc.Hex).substring(0,7)+"_"
    +(process.env.NODE_ENV || 'development').toLowerCase();