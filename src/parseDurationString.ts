/** Parses a time duration string into a number of milliseconds.
  * @param duration The duration string to parse. Supported units:
  * - `ms` - milliseconds
  * - `s` - seconds
  * - `m` - minutes
  * - `h` - hours
  * - `d` - days
  * - `w` - weeks
  *
  * Units must be suffixed directly to the number, e.g. `1d` for 1 day.
  * The number must also be an integer (whole number).
  * @returns The number of milliseconds in the duration, or `undefined` if the duration format is invalid.
  */
function parseTimeDuration(duration: string): number | undefined {
  // This function was made by ChatGPT. I could have just used a library but for this I felt that was unnecessary.
  // I also could have just written it myself, and I've done so in the past, but to be honest I didn't feel like it.
  const timeRegex = /^(\d+)(ms|s|m|h|d|w)$/; // Regular expression pattern

  const match = duration.match(timeRegex); // Match the duration against the pattern

  if (match) {
    const value = parseInt(match[1], 10); // Extract the numeric value
    const unit = match[2]; // Extract the unit

    switch (unit) {
      case 'ms':
        return value;
      case 's':
        return value * 1000;
      case 'm':
        return value * 1000 * 60;
      case 'h':
        return value * 1000 * 60 * 60;
      case 'd':
        return value * 1000 * 60 * 60 * 24;
      case 'w':
        return value * 1000 * 60 * 60 * 24 * 7;
    }
  }

  return undefined; // Return undefined if the duration format is invalid
}