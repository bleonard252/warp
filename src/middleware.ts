import { NextRequest, NextResponse } from 'next/server'
import { authMiddleware } from './middleware/auth';

export const config = {
  matcher: ['/admin', '/admin/**'],
}

export default async function middleware(req: NextRequest) {
  const auth = await authMiddleware(req);
  if (auth != NextResponse.next()) return auth;
  // other middleware here?
  return NextResponse.next();
}