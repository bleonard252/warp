import { ok } from "assert";
import { NextRequest, NextResponse } from "next/server";
import type { UserinfoResponse } from "openid-client";
// import { Issuer } from "openid-client"; // This does not work in middleware!

export async function authMiddleware(req: NextRequest): Promise<NextResponse> {
  if (!(req.nextUrl.pathname.split('/')[1] == 'admin' || req.nextUrl.pathname.split('/')[0] == 'admin')) return NextResponse.next();

  const authHeader = req.headers.get('authorization')
  const authCookie = req.cookies.get('warp_credential')
  const url = req.nextUrl

  if (authHeader) {
    var success = true;
    const authType = authHeader.split(' ')[0];
    try {
      ok(authType === 'Bearer')
    } catch(_) {
      success = false
    }
    const authValue = authHeader.split(' ')[1];

    // cheat by checking this app's API
    const response = await fetch(url.origin+'/api/v1/apikey', {
      headers: new Headers({
        'Authorization': `Bearer ${authValue}`,
      })
    });
    if (response.status == 401) {
      success = false;
    } else if (response.status != 200) {
      return new NextResponse(JSON.stringify({
        "error": "unknown",
        "message": "An unknown error occurred.",
        "technical": response.text(),
      }), {
        status: 500,
      });
    }
    if (authValue && success) {
      return NextResponse.next()
    }
  } else if (authCookie) {
    var success = true;
    var oidconfig: any;
    try {
      oidconfig = await (await fetch(process.env.OPENID_ROOT+"/.well-known/openid-configuration")).json();
    } catch(_) {
      return new NextResponse(JSON.stringify({
        "error": "discovery-failed",
        "message": 'Could not discover OpenID configuration. The auth server may be down.'
      }), {
        status: 500,
      });
    }
    const userinfoResponse = await fetch(oidconfig.userinfo_endpoint, {
      headers: new Headers({
        'Authorization': `Bearer ${authCookie.value}`,
      })
    });
    if (userinfoResponse.status == 401) {
      success = false;
    } else if (userinfoResponse.status != 200) {
      return new NextResponse(JSON.stringify({
        "error": "unknown",
        "message": 'An unknown error occurred. Please try again later.'
      }), {
        status: 500,
      });
    }
    if (success) {
      const userinfo: UserinfoResponse = await userinfoResponse.json();
      if (userinfo) {
        if (process.env.OPENID_CLAIM) {
          if (userinfo[process.env.OPENID_CLAIM]) {
            return NextResponse.next()
          } else {
            return new NextResponse(JSON.stringify({"error": "unauthorized"}), {
              status: 401,
            });
          }
        }
        return NextResponse.next()
      }
    }
  }
  url.pathname = '/api/v1/auth/login';
  url.searchParams.set('redirect', req.nextUrl.pathname);

  return NextResponse.rewrite(url);
}