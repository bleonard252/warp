import { IncomingMessage } from "http";

export function readCookiesFromHeaderValue(headerValue: string): Map<string, string> {
  return new Map<string, string>(headerValue.split(/; ?/).map((cookie) => {
    const [k,v] = cookie.split('=');
    return [k?.trim(), v?.trim()];
  }));
}

export function readCookiesFromHeader(header: string | Headers | IncomingMessage | undefined): Map<string, string | string[]> {
  const cookies = new Map<string, string | string[]>();
  var headerValue: string | undefined | null;
  if (!header) {
    return cookies;
  }
  if (header instanceof IncomingMessage) {
    headerValue = header.headers.cookie;
  }
  if (header instanceof Headers) {
    headerValue = header.get("cookie");
  }
  if (!headerValue) {
    return cookies;
  }
  const cookieStrings = headerValue.split("; ");
  for (const cookieString of cookieStrings) {
    const [name, value] = cookieString.split("=");
    if (cookies.has(name) && cookies.get(name)) {
      const existingValue = cookies.get(name);
      if (existingValue instanceof Array) {
        existingValue.push(value);
        cookies.set(name, existingValue);
      } else {
        cookies.set(name, [existingValue!, value]);
      }
    } else {
      cookies.set(name, value);
    }
  }
  return cookies;
}

/** Set the security features for cookies.
    When running on localhost, all the security features are turned off for ease of development.

    @param suffix If this is set to `true`, put this right after `<cookie-name>=<cookie-value>; ` in the Set-Cookie header,
    and put something IMMEDIATELY after it.

    Example:
    ```js
    `warp_auth_redirect=value; ${setcookieSecurityFeatures()}Max-Age=300;`
    ```

    @param prefix If this is set to `true`, put this right after `<cookie-name>=<cookie-value>` in the Set-Cookie header.
    The `; ` will be appended automatically if security features are engaged.
**/
export const setcookieSecurityFeatures = (suffix: boolean = true, prefix: boolean = false) =>
process.env.PRIMARY_DOMAIN?.startsWith("localhost:") || process.env.PRIMARY_DOMAIN === "localhost" ? ''
: (`${prefix ? '; ' : ''}Domain=${process.env.PRIMARY_DOMAIN}; SameSite=Lax; HttpOnly${suffix ? '; ' : ''}`);
/** Set some lax security features for cookies. Useful to work around redirect-related bugs.
    When running on localhost, all the security features are turned off for ease of development.

    @param suffix If this is set to `true`, put this right after `<cookie-name>=<cookie-value>; ` in the Set-Cookie header,
    and put something IMMEDIATELY after it.

    Example:
    ```js
    `warp_auth_redirect=value; ${setcookieSecurityFeatures()}Max-Age=300;`
    ```

    @param prefix If this is set to `true`, put this right after `<cookie-name>=<cookie-value>` in the Set-Cookie header.
    The `; ` will be appended automatically if security features are engaged.
**/
export const setcookieLaxSecurityFeatures = (suffix: boolean = true, prefix: boolean = false) =>
process.env.PRIMARY_DOMAIN?.startsWith("localhost:") || process.env.PRIMARY_DOMAIN === "localhost" ? ''
: (`${prefix ? '; ' : ''} SameSite=Lax; HttpOnly${suffix ? '; ' : ''}`);