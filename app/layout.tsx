import './globals.css'

import localFont from 'next/font/local';

export const radioCanada = localFont({
  src: './fonts/RadioCanada-Variable.ttf',
  display: 'swap',
});

export const metadata = {
  title: 'Warp!',
  description: 'Blake\'s link shortener',
}

export default function RootLayout({
  children,
}: {
  children: React.ReactNode
}) {
  return (
    <html lang="en">
      <body className={radioCanada.className}>{children}</body>
    </html>
  )
}
