"use client";

import { Button } from '@/components/button';
import { Menu } from '@headlessui/react';
import { usePathname } from 'next/navigation';
import * as React from 'react';
import LucideChevronDown from '~icons/lucide/chevron-down.jsx'
import LucideUser from '~icons/lucide/user.jsx'

export default function Menubar({ openidRoot, accountPage }: { openidRoot?: string, accountPage?: string }) {
  const path = usePathname();
  function isActive(targetPath: string) {
    if (path === targetPath) {
      return { primary: true, filled: true, static: true };
    }
    return {href: targetPath};
  }
  return <div role="menubar" id="menubar" className="p-2 flex flex-row items-center overflow-x-auto overflow-y-visible">
    <h1 className="inline-block text-xl ml-4 mr-4 text-primary-3">Warp!</h1>
    <Button className="mr-2" {...isActive("/admin")}>Links</Button>
    <Button className="mr-2" href="/api/v1/docs/index.html" target="_blank">Docs</Button>
    <Button className="mr-2" {...isActive("/admin/keys")}>API Keys</Button>
    {/* <Button className="mr-2" {...isActive("/admin/super")}>Superuser</Button> */}
    <div className="flex-grow"></div>
    <Menu as="div" className="relative inline-block text-left">
      <Menu.Button className="bg-transparent hover:bg-onscheme-1/25 active:bg-onscheme-1/10 active:text-onscheme-1/75 rounded-2xl text-onscheme-1 p-2 mr-2 flex flex-row" aria-label="Account menu"><LucideChevronDown /><LucideUser /></Menu.Button>

      <Menu.Items className="fixed right-0 z-10 rounded-md m-2 p-2 bg-scheme-3 flex flex-col gap-2">
        {openidRoot?.includes("/realms/") ? <Menu.Item as="a" href={new URL("account", openidRoot).toString()} className="p-2 rounded-md hover:bg-scheme-4/50 truncate">Account settings</Menu.Item>
        : accountPage && <Menu.Item as="a" href={accountPage} className="p-2 rounded-md hover:bg-scheme-4/50 truncate">Account settings</Menu.Item>}

        <Menu.Item as="a" href="/api/v1/auth/logout" className="p-2 rounded-md hover:bg-red-500/30 transition-colors hover:text-red-500 truncate">Log out</Menu.Item>
      </Menu.Items>
    </Menu>
  </div>;
}