import { Button } from '@/components/button'
import '../globals.css'
import Menubar from './menubar'
import { radioCanada } from '../layout'

export const metadata = {
  title: 'Warp! Admin Panel',
  description: 'Shorten links and manage your account',
}

export default function RootLayout({
  children,
}: {
  children: React.ReactNode
}) {
  return (
    <html lang="en">
      <body className={`${radioCanada.className} bg-scheme-1 text-onscheme-1`}>
        <Menubar openidRoot={process.env.OPENID_ROOT} accountPage={process.env.OPENID_ACCOUNT_PAGE} />
        {children}
      </body>
    </html>
  )
}
