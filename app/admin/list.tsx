"use client";

import { Button } from "@/components/button";
import { Fragment, Suspense, lazy, useEffect, useMemo, useState } from "react";
import { Dialog, Transition } from "@headlessui/react";
import { ShortenResponse } from "@/pages/api/v1/shorten";

export default function ShortlinkList() {
  const ShortlinksElement = lazy(() => import('./shortlinks'));
  return <Suspense fallback={<div>Loading...</div>}>
    <ShortlinksElement />
  </Suspense>;
}