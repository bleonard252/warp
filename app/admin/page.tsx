import ShortlinkList from "./list";
import ShortenForm from "./shortenForm";

export default function Home() {
  return (
    <>
      <ShortenForm />
      <ShortlinkList />
    </>
  )
}