"use client";

import { Button } from "@/components/button";
import { Fragment, useEffect, useState } from "react";
import { Dialog, Disclosure, Listbox, Transition } from "@headlessui/react";
import LucideChevronDown from "~icons/lucide/chevron-down.jsx";
import LucideCheck from "~icons/lucide/check.jsx";
import { getDomainList } from "@/pages/api/v1/domains";

export default function CreateKeyForm() {
  const [keyName, setKeyName] = useState('');

  const [accessToken, setAccessToken] = useState('');
  const [clientId, setClientId] = useState('');
  const [isTokenOpen, setIsTokenOpen] = useState(false);

  const [isFailedOpened, setIsFailedOpened] = useState(false);
  const [errorMessage, setErrorMessage] = useState('');

  const [needsAuth, setNeedsAuth] = useState(false);

  async function onSubmit(event: React.FormEvent) {
    event.preventDefault();
    const response = await fetch('/api/v1/apiKey', {
      body: JSON.stringify({
        name: keyName,
      }),
      headers: {
        'Content-Type': 'application/json',
      },
      redirect: 'error',
      method: 'POST',
      credentials: 'include',
    }).catch((e) => {
      console.error(e);
      return new Response(null, { status: 500, statusText: 'Internal Server Error' });
    });
    if (response.ok) {
      setIsTokenOpen(true);
      const json = await response.json();
      setAccessToken(json["token"]);
      setClientId(json["client_id"]);
      setErrorMessage('');
      setIsFailedOpened(false);
      window['updateApiKeyList']?.();
    } else if (response.status == 401) {
      setNeedsAuth(true);
    } else {
      console.error(response);
      try {
        const json = await response.json();
        setErrorMessage(json["message"] || json["error"]);
      } catch (_) {
        setErrorMessage(response.statusText);
      }
      setIsFailedOpened(true);
      setIsTokenOpen(false);
      setAccessToken('');
    }
  }

  return <form action={`/api/v1/apiKey`} method='POST' className='max-w-prose mx-auto mt-4 p-4 flex flex-col gap-2' onSubmit={onSubmit}>
    {needsAuth && <div className="flex flex-col gap-2">
      <h1 className="text-xl">You need to log in first.</h1>
      <Button primary filled flex className="justify-center" onClick={() => {
        const authWindow = window.open('/api/v1/auth/login?redirect=/api/v1/auth/success&noretry=1', 'AuthWindow');
        authWindow?.addEventListener('load', () => {
          if (authWindow.location.pathname == '/api/v1/auth/success') {
            authWindow.close();
            setNeedsAuth(false);
          }
        });
      }}>Log in</Button>
    </div>}
    <input type="hidden" name="_html_response" value="true" />
    <div className='flex flex-row gap-2'>
      <input
        className='w-full flex-grow p-2 rounded-md border-2 border-scheme-2 focus:outline-none focus:border-primary-3 bg-scheme-2'
        placeholder='Name of your new API key'
        type='text'
        minLength={1}
        name='name'
        required={true}
        id='apikey_name'
        value={keyName}
        onChange={(e) => {setKeyName(e.target.value); setAccessToken(''); setClientId(''); setIsTokenOpen(false);}}
      />
      <Button primary filled>Generate&nbsp;Token</Button>
    </div>
    <div className="absolute">
      <Transition
        show={isTokenOpen}
        enter="transition duration-100 ease-out"
        enterFrom="transform opacity-0"
        enterTo="transform opacity-100"
        leave="transition duration-75 ease-out"
        leaveFrom="transform opacity-100"
        leaveTo="transform opacity-0"
        as={Fragment}
      >
        <Dialog open={isTokenOpen} className="absolute top-0 left-0 w-screen h-screen z-50" onClose={() => setIsTokenOpen(false)}>
          <div className="fixed top-0 left-0 w-screen h-screen bg-black/30" aria-hidden="true" />
          <div className="flex flex-row justify-center w-screen">
            <Dialog.Panel className="fixed m-auto mt-12 max-w-full w-[460ch] flex p-8 bg-scheme-2 text-onscheme-2 rounded-md flex-col gap-4">
              <Dialog.Title className="text-3xl">Key? Generated.</Dialog.Title>

              <label className="text-sm">Here&apos;s your token. It will <strong>only be shown once</strong>.</label>
              <input type="url" id="fluffyKitten" className="p-2 rounded-md border-2 border-scheme-2 focus:outline-none focus:border-primary-3 bg-scheme-2" value={accessToken} readOnly />

              <label className="text-sm mt-2">Client ID: {clientId}</label>

              <div className="flex flex-row gap-2 justify-end">
                <Button primary onClick={() => {setIsTokenOpen(false); setAccessToken(''); setClientId('')}}>Thanks</Button>
              </div>
            </Dialog.Panel>
          </div>
        </Dialog>
      </Transition>
      <Transition
        show={isFailedOpened}
        enter="transition duration-100 ease-out"
        enterFrom="transform opacity-0"
        enterTo="transform opacity-100"
        leave="transition duration-75 ease-out"
        leaveFrom="transform opacity-100"
        leaveTo="transform opacity-0"
        as={Fragment}
      >
        <Dialog open={isFailedOpened} className="absolute top-0 left-0 w-screen h-screen z-50" onClose={() => setIsFailedOpened(false)}>
          <div className="fixed top-0 left-0 w-screen h-screen bg-black/30" aria-hidden="true" />
          <div className="flex flex-row justify-center w-screen">
            <Dialog.Panel className="fixed m-auto mt-12 max-w-full w-96 flex p-8 bg-scheme-2 text-onscheme-2 rounded-md flex-col gap-4">
              <Dialog.Title className="text-3xl">Could not create API key.</Dialog.Title>

              <p>{errorMessage}</p>

              <div className="flex flex-row gap-2 justify-end">
                <Button primary onClick={() => {setIsFailedOpened(false); setErrorMessage('');}}>OK</Button>
              </div>
            </Dialog.Panel>
          </div>
        </Dialog>
      </Transition>
    </div>
  </form>;
}