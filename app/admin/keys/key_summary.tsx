"use client";
import { Button } from "@/components/button";
import LucideTrash2 from '~icons/lucide/trash-2.jsx';
import React, { useState } from "react";
import { ApiKeyResponse } from "@/pages/api/v1/apiKey";

export default function ApiKeySummary({ apiKey, setErrorMessage, setIsOk, isBusy, setIsBusy, keyList, setKeyList }: { apiKey: ApiKeyResponse, setErrorMessage: (message: string) => void, setIsOk: (isOk: boolean) => void, isBusy: boolean, setIsBusy: (isBusy: boolean) => void, keyList: ApiKeyResponse[], setKeyList: (keyList: ApiKeyResponse[]) => void }) {
  //const [popup, setPopup] = useContext(dynamicPopup);
  const [keyName, setKeyName] = useState(apiKey.name);

  function onChangeName() {
    if (keyName !== apiKey.name) {
      fetch(`/api/v1/apiKey/${apiKey.client_id}`, {
        credentials: 'include',
        method: 'PUT',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          name: keyName,
        }),
      }).catch((e) => {
        console.error(e);
        return new Response(null, { status: 500, statusText: 'Internal Server Error' });
      }).then((response) => {
        if (!response.ok) {
          if (response.status === 401) {
            window.location.href = '/api/v1/auth/login?redirect=/admin';
            return;
          }
          setErrorMessage(response.statusText);
          setIsOk(false);
          setIsBusy(false);
        } else {
          setKeyList(keyList.map((k) => {
            if (k.client_id === apiKey.client_id) {
              k.name = keyName;
            }
            return k;
          }));
        }
      });
    }
  }

  return <>
    <div className="border border-scheme-2 first:rounded-t-md last:rounded-b-md flex flex-wrap gap-1 relative top-0">
      <div className="absolute right-0">
        {/* <Button className="p-2" onClick={() => {
          setPopup(<ShortlinkDetails link={link} />);
        }}><LucideEdit color="currentColor" /></Button> */}
        <Button className="p-2 text-red-500 enabled:hover:bg-red-500/30 enabled:active:bg-red-500/60" onClick={() => {
          fetch(`/api/v1/apiKey/${apiKey.client_id}`, {
            credentials: 'include',
            method: 'DELETE',
          }).catch((e) => {
            console.error(e);
            return new Response(null, { status: 500, statusText: 'Internal Server Error' });
          }).then((response) => {
            if (!response.ok) {
              if (response.status === 401) {
                window.location.href = '/api/v1/auth/login?redirect=/admin';
                return;
              }
              setErrorMessage(response.statusText);
              setIsOk(false);
              setIsBusy(false);
            } else {
              //setShortlinks(shortlinks.filter((l) => l["link"] !== link["link"]));
              setKeyList(keyList.filter((k) => k.client_id !== apiKey.client_id));
            }
          });
        }}><LucideTrash2 color="currentColor" /></Button>
      </div>
      <div className="p-2 pb-0 w-full">
        <input
          placeholder="API Key Name"
          type="text"
          readOnly={isBusy}
          className="w-full text-lg font-bold text-onscheme-1 bg-transparent border-0"
          value={keyName}
          onChange={(e) => setKeyName(e.target.value)}
          onBlur={onChangeName}
          onKeyDown={(e) => {
            if (e.key === 'Enter') {
              onChangeName();
            }
          }}
        />
      </div>
      {apiKey.created_at && <div className="flex-grow p-2 py-0 w-full text-sm max-h-[2.2rem] whitespace-nowrap text-ellipsis overflow-hidden">
        Created {new Intl.DateTimeFormat("en-US", {dateStyle: "long", timeStyle: "short"}).format(Date.parse(apiKey.created_at.toString()))}
      </div>}
      <div className="flex-grow p-2 pt-0 w-full text-sm max-h-[2.2rem] whitespace-nowrap text-ellipsis overflow-hidden">
        Client ID: {apiKey.client_id}
      </div>
    </div>
  </>;
}