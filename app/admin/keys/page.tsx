import CreateKeyForm from "./createForm";
import ApiKeyList from "./key_list";

export default function Home() {
  return (
    <>
      <CreateKeyForm />
      <ApiKeyList />
    </>
  )
}