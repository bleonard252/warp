"use client";
import { Button } from "@/components/button";
import { ShortenResponse } from "@/pages/api/v1/shorten";
import { createContext, Dispatch, SetStateAction, useEffect, useState } from "react";
import LucideTrash2 from '~icons/lucide/trash-2.jsx';
import ShortlinkSummary from "./key_summary";
import ApiKeySummary from "./key_summary";
import { ApiKeyResponse } from "@/pages/api/v1/apiKey";

export const dynamicPopup = createContext<[JSX.Element, Dispatch<SetStateAction<JSX.Element>>]>([<></>, () => {}]);

export default function ApiKeyList() {
  const popupState = useState(<></>);

  const [isBusy, setIsBusy] = useState(false);
  const [isOk, setIsOk] = useState(true);
  const [errorMessage, setErrorMessage] = useState('');
  const [keyList, setKeyList] = useState<ApiKeyResponse[]>([]);
  const [incrementToReload, setIncrement] = useState(0);

  globalThis?.window && (window['updateApiKeyList'] = () => {
    setIncrement(incrementToReload + 1);
  });

  useEffect(() => {
    setIsBusy(true);
    setIsOk(true); // if this is false, it shows an error. We don't want that.
    fetch('/api/v1/apiKeys', {
      credentials: 'include',
      method: 'GET',
    }).catch((e) => {
      console.error(e);
      return new Response(null, { status: 500, statusText: 'Internal Server Error' });
    }).then((response) => {
      if (!response.ok) {
        if (response.status === 401) {
          window.location.href = '/api/v1/auth/login?redirect=/admin';
          return;
        }
        setErrorMessage(response.statusText);
        setIsOk(false);
        setIsBusy(false);
      }
      response.json().then(setKeyList);
      setIsBusy(false);
      setIsOk(true);
    });
  }, [incrementToReload]);

  return <dynamicPopup.Provider value={popupState}>
    <div className="flex flex-col w-[72ch] m-auto max-w-full">
      {isBusy && <div className="flex flex-col gap-2">
        <div className="flex flex-row gap-2">
          <div className="flex-grow p-2 text-sm">
            <div className="animate-pulse bg-scheme-2 h-8 rounded-md"></div>
          </div>
          <div className="p-2">
            <div className="animate-pulse bg-scheme-2 h-4 rounded-md"></div>
          </div>
        </div>
      </div>}
      {!isOk && <div className="flex flex-col gap-2">
        <h1 className="text-3xl">Error</h1>
        <p className="text-sm">{errorMessage}</p>
      </div>}
      {keyList.reverse().map((key) => <ApiKeySummary apiKey={key} key={key.client_id} setErrorMessage={setErrorMessage} setIsOk={setIsOk} isBusy={isBusy} setIsBusy={setIsBusy} keyList={keyList} setKeyList={setKeyList} />)}
    </div>
    {popupState[0]}
  </dynamicPopup.Provider>;
}