"use client";
import { Button } from "@/components/button";
import { ShortenResponse } from "@/pages/api/v1/shorten";
import LucideTrash2 from '~icons/lucide/trash-2.jsx';
import LucideEdit from '~icons/lucide/edit.jsx';
import { dynamicPopup } from "./shortlinks";
import { Fragment, useContext } from "react";
import ShortlinkDetails from "./link_details";

export default function ShortlinkSummary({ link, setErrorMessage, setIsOk, setIsBusy, shortlinks, setShortlinks }: { link: ShortenResponse; setErrorMessage: (message: string) => void; setIsOk: (isOk: boolean) => void; setIsBusy: (isBusy: boolean) => void; shortlinks: ShortenResponse[]; setShortlinks: (shortlinks: ShortenResponse[]) => void; }) {
  const [popup, setPopup] = useContext(dynamicPopup);
  return <>
    <div className="border border-bottom-none last:border-bottom border-scheme-2 first:rounded-t-md last:rounded-b-md flex flex-wrap gap-1 relative top-0">
      <div className="absolute right-0">
        <Button className="p-2" onClick={() => {
          setPopup(<ShortlinkDetails link={link} />);
        }}><LucideEdit color="currentColor" /></Button>
        <Button className="p-2 text-red-500 enabled:hover:bg-red-500/30 enabled:active:bg-red-500/60" onClick={() => {
          fetch(`/api/v1/link/${link["domain"]}/${link["shortcode"]}`, {
            credentials: 'include',
            method: 'DELETE',
          }).catch((e) => {
            console.error(e);
            return new Response(null, { status: 500, statusText: 'Internal Server Error' });
          }).then((response) => {
            if (!response.ok) {
              if (response.status === 401) {
                window.location.href = '/api/v1/auth/login?redirect=/admin';
                return;
              }
              setErrorMessage(response.statusText);
              setIsOk(false);
              setIsBusy(false);
            } else {
              setShortlinks(shortlinks.filter((l) => l["link"] !== link["link"]));
            }
          });
        }}><LucideTrash2 color="currentColor" /></Button>
      </div>
      <div className="p-2 w-full">
        Shortlink: <a href={link["link"]} className="text-primary-3 hover:underline">{link["link"]}</a>
      </div>
      {/* masked title and description here if set */}
      {link.created_at && <div className="flex-grow p-2 py-0 w-full text-sm max-h-[2.2rem] whitespace-nowrap text-ellipsis overflow-hidden">
        Created {new Intl.DateTimeFormat("en-US", {dateStyle: "long", timeStyle: "short"}).format(Date.parse(link.created_at.toString()))}
      </div>}
      {link.expires_at && <div className="flex-grow p-2 py-0 w-full text-sm max-h-[2.2rem] whitespace-nowrap text-ellipsis overflow-hidden">
        Expiring {new Intl.DateTimeFormat("en-US", {dateStyle: "long", timeStyle: "short"}).format(Date.parse(link.expires_at.toString()))}
      </div>}
      <div className="flex-grow p-2 pt-0 w-full text-sm max-h-[2.2rem] whitespace-nowrap text-ellipsis overflow-hidden">
        <a href={link["long_url"]} title={link["long_url"]} className="text-primary-3 hover:underline">{link["long_url"]}</a>
      </div>
    </div>
  </>;
}
