"use client";
import { Button } from "@/components/button";
import { ShortenResponse } from "@/pages/api/v1/shorten";
import { createContext, Dispatch, Fragment, SetStateAction, useEffect, useState } from "react";
import LucideTrash2 from '~icons/lucide/trash-2.jsx';
import LucideChevronDown from '~icons/lucide/chevron-down.jsx';
import LucideCheck from '~icons/lucide/check.jsx';
import ShortlinkSummary from "./link_summary";
import { Listbox, Transition } from "@headlessui/react";

export const dynamicPopup = createContext<[JSX.Element, Dispatch<SetStateAction<JSX.Element>>]>([<></>, () => {}]);

export default function Shortlinks() {
  const popupState = useState(<></>);

  const [isBusy, setIsBusy] = useState(false);
  const [isOk, setIsOk] = useState(true);
  const [errorMessage, setErrorMessage] = useState('');
  const [shortlinks, setShortlinks] = useState<ShortenResponse[]>([]);
  const [incrementToReload, setIncrement] = useState(0);

  const [filterDomain, setFilterDomain] = useState<string | null>(null);
  const [domains, setDomains] = useState<string[]>([]);

  globalThis?.window && (window['updateShortlinks'] = () => {
    setIncrement(incrementToReload + 1);
  });

  useEffect(() => {
    setIsBusy(true);
    setIsOk(true); // if this is false, it shows an error. We don't want that.
    const _uri = new URL('http://host.com/api/v1/links');
    if (filterDomain) _uri.searchParams.set('domain', filterDomain || '');
    fetch(_uri.toString().replace("http://host.com", ""), {
      credentials: 'include',
      method: 'GET',
    }).catch((e) => {
      console.error(e);
      return new Response(null, { status: 500, statusText: 'Internal Server Error' });
    }).then((response) => {
      if (!response.ok) {
        if (response.status === 401) {
          window.location.href = '/api/v1/auth/login?redirect=/admin';
          return;
        }
        setErrorMessage(response.statusText);
        setIsOk(false);
        setIsBusy(false);
      }
      response.json().then(setShortlinks);
      setIsBusy(false);
      setIsOk(true);
      fetch('/api/v1/domains').then(async (response) => {
        if (response.ok) {
          const json = await response.json();
          setDomains(json["domains"] || json["default"]);
        } else {
          console.error(response);
          try {
            const json = await response.json();
            setErrorMessage(json["message"] || json["error"]);
          } catch (_) {
            setErrorMessage(response.statusText);
            setIsBusy(false);
            setIsOk(false);
          }
        }
      });
    });
  }, [incrementToReload, filterDomain]);

  return <dynamicPopup.Provider value={popupState}>
    <div className="flex flex-col w-[72ch] m-auto max-w-full">
      {isBusy && <div className="flex flex-col gap-2">
        <div className="flex flex-row gap-2">
          <div className="flex-grow p-2 text-sm">
            <div className="animate-pulse bg-scheme-2 h-8 rounded-md"></div>
          </div>
          <div className="p-2">
            <div className="animate-pulse bg-scheme-2 h-4 rounded-md"></div>
          </div>
        </div>
      </div>}
      {!isOk && <div className="flex flex-col gap-2">
        <h1 className="text-3xl">Error</h1>
        <p className="text-sm">{errorMessage}</p>
      </div>}
      <div className={`flex flex-row gap-2${domains.length ? '' : ' hidden'}`}>
        <div className="flex flex-col gap-2">
          <Listbox value={filterDomain} onChange={setFilterDomain}>
            {({ open }) => (
              <div className="relative">
                <Listbox.Label className="text-sm justify-between flex flex-row gap-6">
                  <span>Filter to domain</span>
                  <span className="text-primary-3 hover:underline hover:text-primary-4" onClick={() => setFilterDomain(null)}>Click to clear</span>
                </Listbox.Label>
                <Listbox.Button className="relative w-full border-2 border-scheme-2 cursor-default rounded-md bg-scheme-2 py-2 pl-3 pr-10 text-left shadow-md focus:outline-none focus-visible:border-indigo-500 focus-visible:ring-2 focus-visible:ring-scheme-3 focus-visible:ring-opacity-75 focus-visible:ring-offset-2 focus-visible:ring-offset-orange-300 flex-grow">
                  <span className="block truncate">{filterDomain}</span>
                  <span className="pointer-events-none absolute inset-y-0 right-0 flex items-center pr-2">
                    <LucideChevronDown
                      className={`${
                        open ? 'rotate-180 transform' : ''
                      } h-5 w-5`}
                      aria-hidden="true"
                    />
                  </span>
                </Listbox.Button>
                <Transition
                  as={Fragment}
                  leave="transition ease-in duration-100"
                  leaveFrom="opacity-100"
                  leaveTo="opacity-0"
                >
                  <Listbox.Options className="absolute mt-1 z-10 max-h-60 w-full overflow-auto rounded-md bg-scheme-3 py-1 text-base text-onscheme-3 shadow-lg ring-1 ring-scheme-3 ring-opacity-5 focus:outline-none sm:text-sm">
                    {domains.map((element, index) => (
                      <Listbox.Option
                        key={index}
                        className={({ selected, active }) =>
                          `relative cursor-default select-none py-2 pl-10 pr-4 ${
                            selected ? 'bg-primary-3/10 text-primary-3' : 'text-onscheme-3 hover:bg-scheme-4/25'
                          }${
                            active && !selected ? ' bg-scheme-4/25' :
                            active && selected ? ' bg-primary-3/20' : ''
                          }`
                        }
                        value={element}
                      >
                        {({ selected }) => (
                          <>
                            <span
                              className={`block truncate ${
                                selected ? 'font-medium' : 'font-normal'
                              }`}
                            >
                              {element}
                            </span>
                            {selected ? (
                              <span className="absolute inset-y-0 left-0 flex items-center pl-3 text-primary-3">
                                <LucideCheck className="h-5 w-5" aria-hidden="true" />
                              </span>
                            ) : null}
                          </>
                        )}
                      </Listbox.Option>
                    ))}
                  </Listbox.Options>
                </Transition>
              </div>
            )}
          </Listbox>
        </div>
      </div>
      <div className="flex flex-col">
        {shortlinks.reverse().map((link) => <><ShortlinkSummary link={link} setErrorMessage={setErrorMessage} setIsOk={setIsOk} setIsBusy={setIsBusy} shortlinks={shortlinks} setShortlinks={setShortlinks} /></>)}
      </div>
    </div>
    {popupState[0]}
  </dynamicPopup.Provider>;
}
