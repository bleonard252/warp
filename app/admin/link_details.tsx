import { Button } from "@/components/button";
import { ShortenResponse } from "@/pages/api/v1/shorten";
import { Dialog, Transition } from "@headlessui/react";
import React, { useContext, useState } from "react";
import LucideX from '~icons/lucide/x.jsx';
import LucideCalendar from '~icons/lucide/calendar.jsx';
import { dynamicPopup } from "./shortlinks";
import DateTimePicker from "react-datetime-picker";

export default function ShortlinkDetails({ link }: { link: ShortenResponse }) {
  const [ trueLink, setTrueLink ] = useState(link);

  const [ popup, setPopup ] = useContext(dynamicPopup);

  const [isBusy, setIsBusy] = useState(false);
  const [isOk, setIsOk] = useState(true);
  const [errorMessage, setErrorMessage] = useState('');
  const [needsAuth, setNeedsAuth] = useState(false);

  const [shortcode, setShortcode] = useState(link.shortcode);
  const [longUrl, setLongUrl] = useState(link.long_url);
  const [expiration, setExpiration] = useState(link.expires_at ? new Date(link.expires_at) : null);

  function onSubmitEdits(event: React.FormEvent<HTMLFormElement>) {
    event.preventDefault();
    setIsBusy(true);
    setIsOk(true);
    fetch(`/api/v1/link/${link.domain}/${link.shortcode}`, {
      credentials: 'include',
      method: 'PUT',
      body: JSON.stringify({
        long_url: longUrl || trueLink.long_url,
        shortcode: shortcode || trueLink.shortcode,
        expires_at: expiration?.toISOString() || trueLink.expires_at || undefined,
      }),
      headers: {
        'Content-Type': 'application/json',
      },
    }).then(async (response) => {
      if (response.ok) {
        const json = await response.json();
        setShortcode(json["shortcode"]);
        setLongUrl(json["long_url"]);
        // setDomain(json["domain"]);
        var newLink = {...trueLink};
        newLink.shortcode = json["shortcode"];
        newLink.long_url = json["long_url"];
        newLink.domain = json["domain"];
        newLink.expires_at = json["expires_at"];
        setErrorMessage('');
        setIsBusy(false);
        setIsOk(true);
        setPopup(<></>);
        window['updateShortlinks']?.();
      } else if (response.status == 401) {
        setNeedsAuth(true);
      } else {
        setIsBusy(false);
        setIsOk(false);
        try {
          const json = await response.json();
          setErrorMessage(json["message"] || json["error"]);
        } catch (_) {
          setErrorMessage(response.statusText);
        }
      }
    });
  }

  return <>
    <Transition
      show={true}
      enter="transition-opacity duration-75"
      enterFrom="opacity-0"
      enterTo="opacity-100"
      leave="transition-opacity duration-150"
      leaveFrom="opacity-100"
      leaveTo="opacity-0"
    >
      <Dialog open={true} className="absolute top-0 left-0 w-screen h-screen z-50" onClose={() => setPopup(<></>)}>
        <div className="fixed top-0 left-0 w-screen h-screen bg-black/30" aria-hidden="true" />
        <div className="flex flex-row justify-end w-screen">
          <Dialog.Panel className="fixed m-auto max-w-full w-96 h-screen flex p-8 bg-scheme-2 text-onscheme-2 rounded-md rounded-r-none flex-col gap-4 overflow-y-auto">
            <Button onClick={() => setPopup(<></>)} className="m-4 p-2 fixed top-0 right-0"><LucideX color="currentColor" /></Button>
            <Dialog.Title className="text-2xl">Link details</Dialog.Title>
            <p className="text-sm">Shortlink: <a href={trueLink["link"]} className="text-primary-3 hover:underline">{trueLink["link"]}</a></p>
            {link.created_at && <p className="text-sm">Created {new Intl.DateTimeFormat("en-US", {dateStyle: "long", timeStyle: "short"}).format(Date.parse(link.created_at.toString()))}</p>}

            {needsAuth && <div className="flex flex-col gap-2">
              <h1 className="text-xl">You need to log in first.</h1>
              <Button primary filled flex className="justify-center" onClick={() => {
                const authWindow = window.open('/api/v1/auth/login?redirect=/api/v1/auth/success&noretry=1', 'AuthWindow');
                authWindow?.addEventListener('load', () => {
                  if (authWindow.location.pathname == '/api/v1/auth/success') {
                    authWindow.close();
                    setNeedsAuth(false);
                  }
                });
              }}>Log in</Button>
            </div>}
            {!isOk && <div className="flex flex-col gap-2">
              <h1 className="text-3xl">Error</h1>
              <p className="text-sm">{errorMessage}</p>
            </div>}

            <form method="POST" action={`/api/v1/link/${link.domain}/${link.shortcode}`} className="flex flex-col gap-4" onSubmit={onSubmitEdits}>
              {/* <label htmlFor="domain" className="text-sm">Domain</label> */}
              {/* Domain drop down */}
              <label htmlFor="edit_shortcode" className="flex flex-col gap-1 group">
                <span className="text-sm group-focus-within:text-primary-3">Shortcode</span>
                <input disabled={isBusy} placeholder={trueLink.shortcode} type="text" id="edit_shortcode" name="shortcode" className="p-2 rounded-md border-2 border-scheme-3 focus:outline-none focus:border-primary-3 bg-scheme-1" value={shortcode} onChange={(event) => setShortcode(event.target.value)} />
              </label>
              <label htmlFor="edit_long_url" className="flex flex-col gap-1 group">
                <span className="text-sm group-focus-within:text-primary-3">Destination</span>
                <input disabled={isBusy} placeholder={trueLink.long_url} type="url" id="edit_long_url" name="long_url" className="p-2 rounded-md border-2 border-scheme-3 focus:outline-none focus:border-primary-3 bg-scheme-1" value={longUrl} onChange={(event) => setLongUrl(event.target.value)} />
              </label>
              <label htmlFor='expires_at' className='flex flex-col gap-1 group'>
                <span className="text-sm">Expires at</span>
                {/*<input className='w-full flex-grow p-2 rounded-md border-2 border-scheme-2 focus:outline-none focus:border-primary-3 bg-scheme-2' type='text' name='expires_at' id='expires_at' value={expiration} onChange={(e) => setExpiration(e.target.value)} />*/}
                <DateTimePicker
                  className='x-warp-datetime p-2 rounded-md border-2 border-scheme-3 focus:outline-none focus:border-primary-3 bg-scheme-1'
                  value={expiration} onChange={(v) => setExpiration(v)}
                  name='expires_at' id='expires_at'
                  disableClock={true}
                  clearIcon={<LucideX color="currentColor" />}
                  calendarIcon={<LucideCalendar color="currentColor" />}
                  minDate={new Date(Date.now())}
                />
              </label>
              <Button type="submit" primary>Save</Button>
            </form>
          </Dialog.Panel>
        </div>
      </Dialog>
    </Transition>
  </>
}