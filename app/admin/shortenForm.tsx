"use client";

import { Button } from "@/components/button";
import { Fragment, useEffect, useState } from "react";
import { Dialog, Disclosure, Listbox, Transition } from "@headlessui/react";
import LucideChevronDown from "~icons/lucide/chevron-down.jsx";
import LucideCheck from "~icons/lucide/check.jsx";
import LucideX from "~icons/lucide/x.jsx";
import LucideCalendar from "~icons/lucide/calendar.jsx";
import { getDomainList } from "@/pages/api/v1/domains";
import DateTimePicker from "react-datetime-picker";

import 'react-datetime-picker/dist/DateTimePicker.css';
import 'react-calendar/dist/Calendar.css';
import 'react-clock/dist/Clock.css';

export default function ShortenForm() {
  const [url, setUrl] = useState('');
  const [domain, setDomain] = useState(process.env.PRIMARY_DOMAIN);
  const [shortcode, setShortcode] = useState('');
  const [expiration, setExpiration] = useState<Date|null>(null);

  const [domains, setDomains] = useState(process.env.DOMAINS?.split(',') || [process.env.PRIMARY_DOMAIN]);
  useEffect(() => {
    fetch('/api/v1/domains').then(async (response) => {
      if (response.ok) {
        const json = await response.json();
        setDomains(json["domains"]);
        setDomain(domain => json["default"] || domain);
      } else {
        console.error(response);
        try {
          const json = await response.json();
          setErrorMessage(json["message"] || json["error"]);
        } catch (_) {
          setErrorMessage(response.statusText);
        }
      }
    });
  }, []);

  const [shortenedUrl, setShortenedUrl] = useState('');
  const [isShortenedOpened, setIsShortenedOpened] = useState(false);

  const [isFailedOpened, setIsFailedOpened] = useState(false);
  const [errorMessage, setErrorMessage] = useState('');

  const [needsAuth, setNeedsAuth] = useState(false);

  async function onSubmit(event: React.FormEvent) {
    event.preventDefault(); // or else it would redirect to /api/v1/shorten
    const response = await fetch('/api/v1/shorten', {
      body: JSON.stringify({
        long_url: url,
        domain,
        shortcode: shortcode || undefined,
        expires_at: expiration?.toISOString() || undefined,
      }),
      headers: {
        'Content-Type': 'application/json',
      },
      redirect: 'error',
      method: 'POST',
      credentials: 'include',
    }).catch((e) => {
      console.error(e);
      return new Response(null, { status: 307, statusText: 'Internal Server Error' });
    });
    if (response.ok) {
      setIsShortenedOpened(true);
      const json = await response.json();
      setShortenedUrl(json["link"]);
      setErrorMessage('');
      setIsFailedOpened(false);
      // Reset form
      setUrl('');
      setShortcode('');
      // setDomain(process.env.PRIMARY_DOMAIN); // Better keep this set.
      setExpiration(null);
      window['updateShortlinks']?.();
    } else if (response.status == 401) {
      //window.location.href = '/api/v1/auth/login?redirect=/admin&noretry=1';
      setNeedsAuth(true);
    } else {
      console.error(response);
      try {
        const json = await response.json();
        setErrorMessage(json["message"] || json["error"]);
      } catch (_) {
        setErrorMessage(response.statusText);
      }
      setIsFailedOpened(true);
      setIsShortenedOpened(false);
      setShortenedUrl('');
    }
  }

  return <form action={`/api/v1/shorten`} method='POST' className='max-w-prose mx-auto mt-4 p-4 flex flex-col gap-2' onSubmit={onSubmit}>
    {needsAuth && <div className="flex flex-col gap-2">
      <h1 className="text-xl">You need to log in first.</h1>
      <Button primary filled flex className="justify-center" onClick={() => {
        const authWindow = window.open('/api/v1/auth/login?redirect=/api/v1/auth/success&noretry=1', 'AuthWindow');
        authWindow?.addEventListener('load', () => {
          if (authWindow.location.pathname == '/api/v1/auth/success') {
            authWindow.close();
            setNeedsAuth(false);
          }
        });
      }}>Log in</Button>
    </div>}
    <input type="hidden" name="_html_response" value="true" />
    <div className='flex flex-row gap-2'>
      <input
        className='w-full flex-grow p-2 rounded-md border-2 border-scheme-2 focus:outline-none focus:border-primary-3 bg-scheme-2'
        placeholder='URL to shorten'
        type='url'
        name='long_url'
        id='long_url'
        value={url}
        onChange={(e) => {setUrl(e.target.value); setShortenedUrl(''); setIsShortenedOpened(false);}}
      />
      <Button primary filled>Shorten!</Button>
    </div>
    <Disclosure defaultOpen={false}>
      {({ open }) => (
        <>
          <Disclosure.Button className={`flex w-full justify-between rounded-md ${open ? 'bg-primary-3/20 hover:bg-primary-3/40' : 'bg-scheme-2/50 hover:bg-scheme-3/75'} px-4 py-2 text-left text-sm font-medium text-primary-900 dark:text-primary-100 focus:outline-none focus-visible:ring focus-visible:ring-primary-3 focus-visible:ring-opacity-75`}>
            <span>Advanced options</span>
            <LucideChevronDown
              className={`${
                open ? 'rotate-180 transform' : ''
              } h-5 w-5 text-primary-3`}
              aria-hidden="true"
            />
          </Disclosure.Button>
          <Disclosure.Panel className="p-4 pb-2 rounded-md bg-scheme-2/50">
            <div className='flex flex-row gap-2 align-middle'>
              <Listbox value={domain} onChange={setDomain}>
                {({ open }) => (
                  <div className="relative">
                    <Listbox.Label className="block text-sm">Domain</Listbox.Label>
                    <Listbox.Button className="relative w-full border-2 border-scheme-2 cursor-default rounded-md bg-scheme-2 py-2 pl-3 pr-10 text-left shadow-md focus:outline-none focus-visible:border-indigo-500 focus-visible:ring-2 focus-visible:ring-scheme-3 focus-visible:ring-opacity-75 focus-visible:ring-offset-2 focus-visible:ring-offset-orange-300 flex-grow">
                      <span className="block truncate">{domain}</span>
                      <span className="pointer-events-none absolute inset-y-0 right-0 flex items-center pr-2">
                        <LucideChevronDown
                          className={`${
                            open ? 'rotate-180 transform' : ''
                          } h-5 w-5`}
                          aria-hidden="true"
                        />
                      </span>
                    </Listbox.Button>
                    <Transition
                      as={Fragment}
                      leave="transition ease-in duration-100"
                      leaveFrom="opacity-100"
                      leaveTo="opacity-0"
                    >
                      <Listbox.Options className="absolute mt-1 z-10 max-h-60 w-full overflow-auto rounded-md bg-scheme-3 py-1 text-base text-onscheme-3 shadow-lg ring-1 ring-scheme-3 ring-opacity-5 focus:outline-none sm:text-sm">
                        {domains.map((element, index) => (
                          <Listbox.Option
                            key={index}
                            className={({ selected, active }) =>
                              `relative cursor-default select-none py-2 pl-10 pr-4 ${
                                selected ? 'bg-primary-3/10 text-primary-3' : 'text-onscheme-3 hover:bg-scheme-4/25'
                              }${
                                active && !selected ? ' bg-scheme-4/25' :
                                active && selected ? ' bg-primary-3/20' : ''
                              }`
                            }
                            value={element}
                          >
                            {({ selected }) => (
                              <>
                                <span
                                  className={`block truncate ${
                                    selected ? 'font-medium' : 'font-normal'
                                  }`}
                                >
                                  {element}
                                </span>
                                {selected ? (
                                  <span className="absolute inset-y-0 left-0 flex items-center pl-3 text-primary-3">
                                    <LucideCheck className="h-5 w-5" aria-hidden="true" />
                                  </span>
                                ) : null}
                              </>
                            )}
                          </Listbox.Option>
                        ))}
                      </Listbox.Options>
                    </Transition>
                  </div>
                )}
              </Listbox>
              <span className="align-middle"> / </span>
              <label htmlFor='custom_slug' className='flex-grow flex flex-col items-start'>
                <span className="text-sm">Custom shortcode</span>
                <input className='w-full flex-grow p-2 rounded-md border-2 border-scheme-2 focus:outline-none focus:border-primary-3 bg-scheme-2' type='text' name='shortcode' id='custom_slug' value={shortcode} onChange={(e) => setShortcode(e.target.value)} />
              </label>
            </div>
            <div className='flex flex-row gap-2 align-middle'>
              <label htmlFor='expires_at' className='flex-grow flex flex-col items-start'>
                <span className="text-sm">Expires at</span>
                {/*<input className='w-full flex-grow p-2 rounded-md border-2 border-scheme-2 focus:outline-none focus:border-primary-3 bg-scheme-2' type='text' name='expires_at' id='expires_at' value={expiration} onChange={(e) => setExpiration(e.target.value)} />*/}
                <DateTimePicker
                  className='x-warp-datetime w-full flex-grow p-2 rounded-md border-2 border-scheme-2 focus:outline-none focus:border-primary-3 bg-scheme-2'
                  value={expiration} onChange={(v) => setExpiration(v)}
                  name='expires_at' id='expires_at'
                  disableClock={true}
                  clearIcon={<LucideX color="currentColor" />}
                  calendarIcon={<LucideCalendar color="currentColor" />}
                  minDate={new Date(Date.now())}
                />
              </label>
            </div>
          </Disclosure.Panel>
        </>
      )}
    </Disclosure>
    <div className="absolute">
      <Transition
        show={isShortenedOpened}
        enter="transition duration-100 ease-out"
        enterFrom="transform opacity-0"
        enterTo="transform opacity-100"
        leave="transition duration-75 ease-out"
        leaveFrom="transform opacity-100"
        leaveTo="transform opacity-0"
        as={Fragment}
      >
        <Dialog open={isShortenedOpened} className="absolute top-0 left-0 w-screen h-screen z-50" onClose={() => setIsShortenedOpened(false)}>
          <div className="fixed top-0 left-0 w-screen h-screen bg-black/30" aria-hidden="true" />
          <div className="flex flex-row justify-center w-screen">
            <Dialog.Panel className="fixed m-auto mt-12 max-w-full w-96 flex p-8 bg-scheme-2 text-onscheme-2 rounded-md flex-col gap-4">
              <Dialog.Title className="text-3xl">Link? Shortened.</Dialog.Title>

              <label className="text-sm">Here&apos;s it short:</label>
              <input type="url" id="shortenedUrl" className="p-2 rounded-md border-2 border-scheme-2 focus:outline-none focus:border-primary-3 bg-scheme-2" value={shortenedUrl} readOnly />

              <div className="flex flex-row gap-2 justify-end">
                <Button primary onClick={() => {setIsShortenedOpened(false); setShortenedUrl('')}}>Thanks</Button>
              </div>
            </Dialog.Panel>
          </div>
        </Dialog>
      </Transition>
      <Transition
        show={isFailedOpened}
        enter="transition duration-100 ease-out"
        enterFrom="transform opacity-0"
        enterTo="transform opacity-100"
        leave="transition duration-75 ease-out"
        leaveFrom="transform opacity-100"
        leaveTo="transform opacity-0"
        as={Fragment}
      >
        <Dialog open={isFailedOpened} className="absolute top-0 left-0 w-screen h-screen z-50" onClose={() => setIsFailedOpened(false)}>
          <div className="fixed top-0 left-0 w-screen h-screen bg-black/30" aria-hidden="true" />
          <div className="flex flex-row justify-center w-screen">
            <Dialog.Panel className="fixed m-auto mt-12 max-w-full w-96 flex p-8 bg-scheme-2 text-onscheme-2 rounded-md flex-col gap-4">
              <Dialog.Title className="text-3xl">Could not shorten link.</Dialog.Title>

              <p>{errorMessage}</p>

              <div className="flex flex-row gap-2 justify-end">
                <Button primary onClick={() => {setIsFailedOpened(false); setErrorMessage('');}}>OK</Button>
              </div>
            </Dialog.Panel>
          </div>
        </Dialog>
      </Transition>
    </div>
  </form>;
}