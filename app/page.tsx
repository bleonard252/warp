/* eslint-disable @next/next/no-html-link-for-pages */
import Image from 'next/image'

export default function Home() {
  return (
    <>
      <video src="https://invidious.snopyta.org/latest_version?id=UQgBVsbbKRs" autoPlay muted nocontrols="true" poster={new URL('./warpspeed_thumbnail.jpg', import.meta.url).toString()} id="background-video"></video>
      <div id="center-info" className="prose">
        <h1 className="text-4xl font-semibold">Warp!</h1>
        <p>This is Blake&apos;s link shortener.</p>
        <p>You can:</p>
        <ul>
          <li><a href={`${process.env.PRIMARY_DOMAIN ? `https://${process.env.PRIMARY_DOMAIN}` : ''}/api/v1/auth/login`} className="underline">Log in to the dashboard</a></li>
          <li><a href={`${process.env.PRIMARY_DOMAIN ? `https://${process.env.PRIMARY_DOMAIN}` : ''}/api/v1/docs/${"index.html"}`} className="underline">Read the API docs</a></li>
          <li><a href="https://codeberg.org/bleonard252/warp" className="underline">See the source code</a></li>
          <li><a href="https://blakes.dev" className="underline">Go home</a></li>
        </ul>
      </div>
    </>
  )
}

declare module 'react' {
  interface HTMLAttributes<T> extends AriaAttributes, DOMAttributes<T> {
    nocontrols?: boolean | string
  }
}