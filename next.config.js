/** @type {import('next').NextConfig} */
const nextConfig = {
  rewrites: () => Promise.resolve([
    {
      "source": "/:code((?!admin|api).+)",
      "destination": "/api/v1/warp/default/:code",
    }
  ]),
  webpack(config) {
    config.plugins.push(
      require('unplugin-icons/webpack')({
        compiler: 'jsx',
        jsx: 'react',
      }),
    );
    return config;
  }
}

module.exports = nextConfig
