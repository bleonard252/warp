/** @type {import('tailwindcss').Config} */

function withOpacityValue(variable) {
  return ({ opacityValue }) => {
    if (opacityValue === undefined) {
      return `rgb(var(${variable}))`
    }
    return `rgb(var(${variable}) / ${opacityValue})`
  }
}
function colorFamily(name) {
  return ({ opacityValue, shade }) => {}
}

module.exports = {
  content: [
    './pages/**/*.{js,ts,jsx,tsx,mdx}',
    './components/**/*.{js,ts,jsx,tsx,mdx}',
    './app/**/*.{js,ts,jsx,tsx,mdx}',
  ],
  theme: {
    extend: {
      backgroundImage: {
        'gradient-radial': 'radial-gradient(var(--tw-gradient-stops))',
        'gradient-conic':
          'conic-gradient(from 180deg at 50% 50%, var(--tw-gradient-stops))',
      },
      colors: {
        mint: {
          50: '#DAF2E3',
          100: '#BBDBC7',
          200: '#9EC3AD',
          300: '#83AB93',
          400: '#6B947B',
          500: '#547C64',
          600: '#40654F',
          700: '#2D4D3A',
          800: '#1D3627',
          900: '#0F1E15',
        },
        scheme: {
          1: withOpacityValue('--scheme-1'),
          2: withOpacityValue('--scheme-2'),
          3: withOpacityValue('--scheme-3'),
          4: withOpacityValue('--scheme-4'),
          5: withOpacityValue('--scheme-5'),
        },
        primary: {
          1: withOpacityValue('--primary-1'),
          2: withOpacityValue('--primary-2'),
          3: withOpacityValue('--primary-3'),
          4: withOpacityValue('--primary-4'),
          5: withOpacityValue('--primary-5'),
        },
        onscheme: {
          1: withOpacityValue('--onscheme-1'),
          2: withOpacityValue('--onscheme-2'),
          3: withOpacityValue('--onscheme-3'),
          4: withOpacityValue('--onscheme-4'),
          5: withOpacityValue('--onscheme-5'),
        },
        onprimary: {
          1: withOpacityValue('--onprimary-1'),
          2: withOpacityValue('--onprimary-2'),
          3: withOpacityValue('--onprimary-3'),
          4: withOpacityValue('--onprimary-4'),
          5: withOpacityValue('--onprimary-5'),
        },
      }
    },
  },
  plugins: [],
}
